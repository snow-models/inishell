//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2019 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * This file includes all our custom panels and provides an object factory for them.
 * 2019-10
 */

#ifndef PANELS_H
#define PANELS_H

#include <src/panels/Checklist.h>
#include <src/panels/Checkbox.h>
#include <src/panels/Choice.h>
#include <src/panels/Copytext.h>
#include <src/panels/Datepicker.h>
#include <src/panels/Dropdown.h>
#include <src/panels/FilePath.h>
#include <src/panels/Group.h>
#include <src/panels/GridPanel.h>
#include <src/panels/Helptext.h>
#include <src/panels/HorizontalPanel.h>
#include <src/panels/ImageView.h>
#include <src/panels/Label.h>
#include <src/panels/Number.h>
#include <src/panels/Replicator.h>
#include <src/panels/Selector.h>
#include <src/panels/Spacer.h>
#include <src/panels/Textfield.h>
#include <src/main/constants.h>

#include <QString>
#include <QWidget>
#include <QtXml>

QWidget * elementFactory(const QString &in_identifier, const QString &section, const QString &key,
    const QDomNode &options, const bool &no_spacers, QWidget *parent = nullptr);

#endif //PANELS_H
