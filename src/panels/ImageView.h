//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2021 Avalanche Warning Service Tyrol                LWD-TIROL  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Show image on an Inishell page.
 * 2021-06
 */

#ifndef IMAGEVIEW_H
#define IMAGEVIEW_H

#include <src/panels/Atomic.h>

#include <QLabel>
#include <QMouseEvent>
#include <QString>
#include <QWidget>
#include <QtXml>

class ImageView : public Atomic {
	Q_OBJECT

	public:
		explicit ImageView(const QString &section, const QString &key, const QDomNode &options,
			const bool &no_spacers, QWidget *parent = nullptr);

	protected:
		void mouseReleaseEvent(QMouseEvent *event) override;

	private:
		QLabel *image_label_ = nullptr;
		QLabel *caption_label_ = nullptr;
		QString url_;

		void setOptions(const QDomNode &options);
		bool loadImage(const QString &path);
};

#endif //IMAGEVIEW_H
