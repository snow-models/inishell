//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2021 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Panels that can create an arbitrary number of child panels are termed "dynamic panels". They can, for
 * example, create enumerated keys like STATION1, STATION2, ... on the fly and their logic is handled here.
 * 2021-03
 */

#ifndef DYNAMIC_PANELS_H
#define DYNAMIC_PANELS_H

#include <src/main/INIParser.h>

#include <QString>
#include <QWidget>
#include <QWidgetList>
#include <QtXml>

bool splitRepKey(const QString &full_key, QString &pre, int &extracted_no, QString &post,
	const bool &rightmost = true);
QString getRepName(const QString &full_key, const bool &multi = true, const bool &has_section=false);
bool prepareReplicator(QWidget *parent, const Section &section, const KeyValue &keyval);
QWidgetList prepareSelector(QWidget *parent, const Section &section, const KeyValue &keyval);
QStringList getAllKeys(const QString &section, const QDomNode &node);
bool hasReplicatorChildren(const QDomNode &node);
void substituteKeys(QDomElement &parent_element, const QString &replace,
	const QString &replace_with, const bool &once = false);
template <class T> void clearDynamicPanels();

#endif //DYNAMIC_PANELS_H
