//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2019 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * The Label is used to display INI keys, but can also stand alone.
 */

#ifndef LABEL_H
#define LABEL_H

#include <src/panels/Atomic.h>

#include <QLabel>
#include <QString>
#include <QTextEdit>
#include <QWidget>
#include <QtXml>

class Label : public Atomic {
	Q_OBJECT

	public:
		explicit Label(const QString &section, const QString &key, const QDomNode &options,
		    const bool &no_spacers, const QString &in_label = QString(), QWidget *parent = nullptr);
		bool isEmpty();

		QLabel *label_ = nullptr; //access from outside for styling

	private:
		int getColumnWidth(const QString &text, const int& min_width);

		QTextEdit *textbox_ = nullptr;

};

#endif //LABEL_H
