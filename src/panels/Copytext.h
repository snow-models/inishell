//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2021 Avalanche Warning Service Tyrol                LWD-TIROL  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * A Copytext displays a piece of text that will be easy to copy/paste.
 * 2021-05
 */

#ifndef COPYTEXT_H
#define COPYTEXT_H

#include <src/panels/Atomic.h>
#include <src/main/SyntaxHighlighter.h>

#include <QLabel>
#include <QString>
#include <QTextEdit>
#include <QToolButton>
#include <QWidget>

class Copytext : public Atomic {
	Q_OBJECT

	public:
		explicit Copytext(const QString &section, const QString &key, const QDomNode &options,
			const bool &no_spacers, QWidget *parent = nullptr);
		~Copytext() override;

	private:
		QTextEdit *textbox_ = nullptr;
		QToolButton *copy_button_ = nullptr;
		QLabel *copy_label_ = nullptr;
		QTimer *clear_timer = nullptr;
		SyntaxHighlighter *syntax_highlighter_ = nullptr;

		void setOptions(const QDomNode &options);

	private slots:
		void onCopyButtonClicked();

};

#endif //COPYTEXT_H
