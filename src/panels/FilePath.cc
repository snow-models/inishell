//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2019 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <src/gui/RememberDialog.h>
#include <src/panels/FilePath.h>
#include <src/panels/Label.h>
#include <src/main/colors.h>
#include <src/main/common.h>
#include <src/main/constants.h>
#include <src/main/dimensions.h>
#include <src/main/Error.h>
#include <src/main/inishell.h>
#include <src/main/os.h>
#include <src/main/settings.h>

#include <QAction>
#include <QDesktopServices>
#include <QFileDialog>
#include <QFileInfo>
#include <QHBoxLayout>
#include <QStringList>

#ifdef DEBUG
	#include <iostream>
#endif //def DEBUG


/**
 * @class FilePreview
 * @brief Default constructor for a file preview window.
 * @param[in] file_path Path to file to show a preview for.
 * @param[in] parent The parent widget.
 */
FilePreview::FilePreview(QString file_path, QWidget *parent) : QWidget(parent), file_path_(file_path)
{
	auto *main_layout( new QHBoxLayout );

	prev_edit_ = new PreviewEdit(true, false);
	prev_edit_->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(prev_edit_, &QWidget::customContextMenuRequested, this, &FilePreview::onContextMenuRequest);
	this->setWindowTitle(tr("Preview") + " \"" + os::prettyPath(file_path_) + "\" ~ " + QCoreApplication::applicationName());

	QTextOption option;
	option.setFlags(QTextOption::ShowLineAndParagraphSeparators | QTextOption::ShowTabsAndSpaces |
		QTextOption::ShowDocumentTerminator);
	prev_edit_->document()->setDefaultTextOption(option);
	//so if the setting does not exists, it will be the same as TRUE
	prev_edit_->setSyntaxHighlighting(getSetting("user::popup_preview::show_syntax_highlighting", "value") != "FALSE");
	const bool line_wrapping( getSetting("user::popup_preview::line_wrap", "value") == "TRUE" );
	prev_edit_->setLineWrapMode(line_wrapping? QPlainTextEdit::WidgetWidth : QPlainTextEdit::NoWrap);

	displayFile(file_path_, Cst::line_nr_normal); //default: show this many lines only

	main_layout->addWidget(prev_edit_);
	main_layout->setContentsMargins(0, 0, 0, 0);
	this->setLayout(main_layout);

	this->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::Dialog); //prevent double title bar on macOS
	this->setAttribute(Qt::WA_DeleteOnClose); //immediately unload window when closing
	dim::setDimensions(this, dim::window_type::FILE_PREVIEW);
}

/**
 * @brief Destructor for the FilePreview popup window.
 * @details Unlike main windows where we only have one of each and the sizes are remembered
 * on program exit, here every preview window contributes its size when it's closed.
 */
FilePreview::~FilePreview()
{
	setSetting("auto::sizes::window_" + QString::number(dim::window_type::FILE_PREVIEW),
		"width", QString::number(this->width()));
	setSetting("auto::sizes::window_" + QString::number(dim::window_type::FILE_PREVIEW),
		"height", QString::number(this->height()));
}

/**
 * @brief Event listener for the ESC key.
 * @details Close the preview on pressing ESC.
 * @param[in] event The key press event that is received.
 */
void FilePreview::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Escape || keyToSequence(event) == QKeySequence::Close)
		this->close();
}

/**
 * @brief Show a file's contents in the text box.
 * @details This function allows to set the number of lines to read from the file in order
 * to prevent mishaps with huge data files.
 * @param[in] file Path to file to read.
 * @param[in] nr_lines Number of lines to show.
 */
void FilePreview::displayFile(const QString &file, const int &nr_lines)
{
	QFile prev_file(file);
	if (!prev_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		prev_edit_->clear(); //clear on subsequent reading errors
		prev_edit_->appendHtml(html::emph(tr("Error reading file: ") + prev_file.errorString()));
		prev_file.close();
		return;
	}

	if (nr_lines == 0) { //0 means whole file
		if (getSetting("user::warnings::warn_popup_preview_huge_file", "value") == "TRUE") { //has the user disabled the warning?
			if (prev_file.size() > Cst::warn_file_size) { //warn on huge files
#if QT_VERSION < QT_VERSION_CHECK(5, 10, 0)
				const QString msg( tr("File size is %1").arg( prev_file.size() ) ); //not that nice, but better than nothing
#else
				QLocale locale( this->locale() );
				const QString msg( tr("File size is %1").arg(locale.formattedDataSize(prev_file.size())) );
#endif
				const QString informative( tr("Do you really want to view this huge file?") );
				const QString details( tr(
					"It may be a good idea to disable syntax highlighting first, as this will slow down the read-in.") );
				const QFlags<QMessageBox::StandardButton> buttons( QMessageBox::Ok | QMessageBox::Cancel );
				RememberDialog msgOpenHuge("user::warnings::warn_popup_preview_huge_file", msg, informative, details, buttons, this);

				const int clicked = msgOpenHuge.exec();
				if (clicked == QMessageBox::Cancel) {
					prev_file.close();
					return;
				}
			} //endif prev_file.size
		} //endif getSetting
#if QT_VERSION < QT_VERSION_CHECK(5, 10, 0)
		QByteArray tmp( prev_file.readAll() );
		tmp.chop(1); //don't add newline that's not in file
		prev_edit_->setPlainText(tmp);
#else
		prev_edit_->setPlainText(prev_file.readAll().chopped(1)); //don't add newline that's not in file
#endif
	} else {
		int counter = 0;
		QTextStream text_data(&prev_file);
		QString collected_lines;
		while (!text_data.atEnd() && counter < nr_lines)
		{
			collected_lines += text_data.readLine() + "\n";
			counter++;
		}
		collected_lines.chop(1); //last newline
		prev_edit_->setPlainText(collected_lines);
		if (!text_data.atEnd())
			//color is effective without syntax highlighting (with it we have a rule for that):
			prev_edit_->appendHtml(html::color(html::emph(tr("(truncated - right-click to show more)")), "warning"));
	}
	prev_file.close();
	prev_edit_->setProperty("nr_lines", nr_lines); //to check right box of temporary context menu
}

/**
 * @brief Show an extended context menu for the file preview editor.
 * @param[in] pos Mouse click position.
 */
void FilePreview::onContextMenuRequest(const QPoint &pos)
{
	QMenu *std_menu( prev_edit_->createStandardContextMenu(pos) );
	std_menu->addSeparator();
	if (file_path_.endsWith(".ini", Qt::CaseInsensitive))
		std_menu->addAction(tr("Open in INI Preview Editor"));
	std_menu->addAction(tr("Open in external editor"));
	auto *syntax_action( new QAction(tr("Syntax highlighting"), std_menu) );
	syntax_action->setCheckable(true);
	//so if the setting does not exists, it will be the same as TRUE
	syntax_action->setChecked(getSetting("user::popup_preview::show_syntax_highlighting", "value") != "FALSE");
	std_menu->addAction(syntax_action);
	auto *line_wrap( new QAction(tr("Line wrapping"), std_menu) );
	line_wrap->setCheckable(true);
	line_wrap->setChecked(getSetting("user::popup_preview::line_wrap", "value") == "TRUE");
	std_menu->addAction(line_wrap);
	std_menu->addSeparator();

	auto *show_lines_normal( new QAction(tr("Show %1 lines").arg(Cst::line_nr_normal), std_menu) );
	show_lines_normal->setProperty("nr_lines", Cst::line_nr_normal);
	show_lines_normal->setCheckable(true);
	//on file display we remember which option was picked to re-create the menu here:
	show_lines_normal->setChecked(prev_edit_->property("nr_lines").toInt() == Cst::line_nr_normal);
	std_menu->addAction(show_lines_normal);
	auto *show_lines_more( new QAction(tr("Show %1 lines").arg(Cst::line_nr_more), std_menu) );
	show_lines_more->setProperty("nr_lines", Cst::line_nr_more);
	show_lines_more->setCheckable(true);
	show_lines_more->setChecked(prev_edit_->property("nr_lines").toInt() == Cst::line_nr_more);
	std_menu->addAction(show_lines_more);
	auto *show_lines_all( new QAction(tr("Show all data"), std_menu) );
	show_lines_all->setProperty("nr_lines", 0);
	show_lines_all->setCheckable(true);
	show_lines_all->setChecked(prev_edit_->property("nr_lines").toInt() == 0);
	std_menu->addAction(show_lines_all);
	std_menu->addSeparator();
	std_menu->addAction( new QAction(tr("File info"), std_menu) );

	QAction *selected( std_menu->exec(QCursor::pos()) );
	if (selected) {
		if (selected->text() == tr("Open in INI Preview Editor")) {
			auto *ini_editor( getMainWindow()->getPreviewEditor() );
			ini_editor->addIniTab(file_path_);
			this->close(); //end "always on top"
			ini_editor->show();
			ini_editor->raise();
		} else if (selected->text() == tr("Open in external editor")) {
			this->close();
			QDesktopServices::openUrl(QUrl(file_path_));
		} else if (selected->text() == tr("Syntax highlighting")) {
			prev_edit_->setSyntaxHighlighting(selected->isChecked());
			setSetting("user::popup_preview::show_syntax_highlighting", "value", selected->isChecked()? "TRUE" : "FALSE");
		} else if (selected->text() == tr("Line wrapping")) {
			prev_edit_->setLineWrapMode(selected->isChecked()? QPlainTextEdit::WidgetWidth : QPlainTextEdit::NoWrap);
			setSetting("user::popup_preview::line_wrap", "value", selected->isChecked()? "TRUE" : "FALSE");
		} else if (selected->text().startsWith("Show")) {
			show_lines_normal->setChecked(false);
			show_lines_more->setChecked(false);
			show_lines_all->setChecked(false);
			selected->setChecked(true);
			displayFile(file_path_, selected->property("nr_lines").toInt());
		} else if (selected->text() == tr("File info")) {
			const QFileInfo prev_file(file_path_);
#if QT_VERSION < QT_VERSION_CHECK(5, 10, 0)
			QString finfo( tr("File size: %1").arg( prev_file.size() ) );
			QString fdetails;
#else
			QLocale locale( this->locale() );
			QString finfo( tr("File size: %1").arg(locale.formattedDataSize(prev_file.size())) );
			QString fdetails( tr("Created on: %1").arg(prev_file.birthTime().toString()) );
#endif
			finfo += tr("\n\nPath: %1").arg(prev_file.filePath());
			fdetails += tr("\nLast modified: %1").arg(prev_file.lastModified().toString());
			fdetails += tr("\nOwned by: %1").arg(prev_file.owner());
			fdetails += tr("\nWriteable: %1").arg(prev_file.isWritable()? tr("yes") : tr("no"));
			messageBox(prev_file.fileName(), finfo, fdetails, error::info, true, true);
		}
	} //endif selected
}

/**
 * @class FilePath
 * @brief Default constructor for a file/path picker.
 * @details The file/path picker displays a dialog to select either a file or a folder.
 * @param[in] section INI section the controlled value belongs to.
 * @param[in] key INI key corresponding to the value that is being controlled by this file/path picker.
 * @param[in] options XML node responsible for this panel with all options and children.
 * @param[in] no_spacers Keep a tight layout for this panel.
 * @param[in] parent The parent widget.
 */
FilePath::FilePath(const QString &section, const QString &key, const QDomNode &options, const bool &no_spacers,
    QWidget *parent) : Atomic(section, key, parent)
{
	/* label and text field */
	auto *key_label( new Label(QString(), QString(), options, no_spacers, key_, this) );
	setEmphasisWidget(key_label->label_);
	path_text_ = new QLineEdit; //textfield with one line
	connect(path_text_, &QLineEdit::textEdited, this, &FilePath::checkValue);
	focus_filter_ = new FocusEventFilter;
	path_text_->installEventFilter(focus_filter_); //select all text on focus receive

	/* "open" and "preview" buttons and info label */
	open_button_ = new QPushButton("…");
	open_button_->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	connect(open_button_, &QPushButton::clicked, this, &FilePath::openFile);
	preview_button_ = new QToolButton;
	preview_button_->setIcon(getIcon("document-preview"));
	preview_button_->setToolTip(tr("Show preview"));
	preview_button_->setEnabled(false); //stays disabled until text is entered
	connect(preview_button_, &QToolButton::clicked, this, &FilePath::previewClicked);
	info_text_ = new QLabel();
	info_text_->setAlignment(Qt::AlignCenter);
	QPalette label_palette( info_text_->palette() ); //custom color
	label_palette.setColor(QPalette::WindowText, colors::getQColor("warning"));
	info_text_->setPalette(label_palette);
	info_text_->setVisible(false);

	/* button to show if this panel is linked to another one */
	link_button_ = new QToolButton;
	link_button_->hide();
	link_button_->setStyleSheet("* {border: none}");
	link_button_->setCursor(Qt::PointingHandCursor);
	link_button_->setFocusPolicy(Qt::NoFocus); //to be the same as in Textfield
	QSize sz_label;
	sz_label.setWidth(fontMetrics().boundingRect(Cst::u_link).width());
	sz_label.setHeight(fontMetrics().boundingRect(Cst::u_link).height());
	link_button_->setFixedSize(sz_label);
	link_button_->setText(Cst::u_link);
	connect(link_button_, &QToolButton::clicked, this, &FilePath::onLinkButtonClicked);

	/* layout of the basic elements */
	auto *filepath_layout( new QHBoxLayout );
	filepath_layout->addWidget(key_label, 0, Qt::AlignLeft);
	filepath_layout->addWidget(path_text_);
	filepath_layout->addWidget(link_button_);
	filepath_layout->addWidget(open_button_);
	filepath_layout->addWidget(preview_button_);
	path_text_->setMinimumWidth(no_spacers? Cst::tiny : Cst::width_filepath_min);
	addHelp(filepath_layout, options, no_spacers);

	/* main layout with interactive widgets and info label */
	auto *main_layout( new QVBoxLayout );
	main_layout->addLayout(filepath_layout);
	main_layout->addWidget(info_text_, 0, Qt::AlignLeft);
	setLayoutMargins(main_layout);
	this->setLayout(main_layout);

	setOptions(options); //file_and_path, filename or path
	path_text_->setPlaceholderText(path_only_? tr("<no path set>") : tr("<no file set>"));
}

/**
 * @brief Default destructor with minimal cleanup.
 */
FilePath::~FilePath()
{
	delete focus_filter_;
}

/**
 * @brief Parse options for a file/patch picker from XML.
 * @param[in] options XML node holding the file/path picker.
 */
void FilePath::setOptions(const QDomNode &options)
{
	const QString type(options.toElement().attribute("type"));
	if ( type == "path") {
		path_only_ = true;
		open_button_->setToolTip(tr("Open path"));
	} else {
		if (type == "filename") {
			filename_only_ = true;
			base_path_ = options.toElement().attribute("path");
			if (!base_path_.isEmpty()) {
				//at this point we do not care about the value of a linked path panel, but we find it
				//once nonetheless to link it visually:
				QStringList referenced_ids;
				const QString tmp_path( base_path_ );
				QList<Atomic *> found_panels;
				pathsub(tmp_path, referenced_ids, found_panels);
				if (referenced_ids.isEmpty()) {
					link_button_->hide();
				} else {
					link_button_->setProperty("link_id", referenced_ids.at(0));
					link_button_->setToolTip(tr(R"(Path of file linked to "%1")").arg(referenced_ids.at(0)));
					link_button_->show();
					//for details on next paragraph cf. onBasePathChanged():
					for (auto &panel : found_panels) {
						//note that multiple active panels for the same key are "semi-unsupported", so we try our best and connect all:
						if (FilePath *found_fp = qobject_cast<FilePath *>(panel) )
							connect(found_fp->getPathLineEdit(), &QLineEdit::textChanged, this, &FilePath::onBasePathChanged);
	#ifdef DEBUG
						else
							std::cout << "Object conversion to FilePath failed for " << referenced_ids.at(0).toStdString() << std::endl;
	#endif //def DEBUG
					}

				}
			} else {
				link_button_->hide();
			} //endif base_path
		}
		open_button_->setToolTip(tr("Open file"));
	}

	if ((options.toElement().attribute("preview").toLower() == "false") | path_only_)
		preview_button_->setVisible(false);

	for (QDomElement op = options.firstChildElement("option"); !op.isNull(); op = op.nextSiblingElement("option")) {
		const QString ext( op.attribute("extension") ); //selectable file extensions can be set
		if (!ext.isEmpty())
			extensions_ += ext + ";;";
	}
	if (extensions_.isEmpty())
		extensions_= tr("All Files (*)");
	else
		extensions_.chop(2); //remove trailing ;;

	if (options.toElement().attribute("mode") == "input") //so we can do a little more checking
		io_mode = INPUT;
	else if (options.toElement().attribute("mode").toLower() == "output")
		io_mode = OUTPUT;

	setDefaultPanelStyles(path_text_->text());
}

/**
 * @brief Helper function to process paths before passing them on.
 * @details This function first performs %SECTION::KEY substitutions,
 * and if a path remains adds a directory separator to it.
 * @param[in] path Path to perform substitutions for.
 * @param[out] captures List of IDs that were replaced by their key's value.
 * @param[out] found_panels List of pointers to the panels that were retrieved while searching.
 * @return Final output path.
 */
QString FilePath::pathsub(const QString &path, QStringList &captures, QList<Atomic *> &found_panels)
{
	QString sub_path( path );
	keysub(sub_path, captures, found_panels);
	if (!sub_path.isEmpty() && !sub_path.endsWith("/"))
		sub_path += "/";
	return sub_path;
}

/**
 * @brief Wrapper for pathsub if we don't need the replaced IDs.
 * @param[in] path Path to perform substitutions for.
 * @return Final output path.
 */
QString FilePath::pathsub(const QString &path)
{
	QStringList captures; //discarded
	QList<Atomic *> found_panels; //discarded
	QString sub_path( path );
	keysub(sub_path, captures, found_panels);
	if (!sub_path.isEmpty() && !sub_path.endsWith("/"))
		sub_path += "/";
	return sub_path;
}

/**
 * @brief Event listener for changed INI values.
 * @details The "ini_value" property is set when parsing default values and potentially again
 * when setting INI keys while parsing a file.
 */
void FilePath::onPropertySet()
{
	const QString filename( this->property("ini_value").toString() );
	if (ini_value_ == filename)
		return;
	checkValueAndSet(filename);
}


void FilePath::checkValueAndSet(const QString &filename)
{
	path_text_->setText(filename);
	checkValue(filename);
}
/**
 * @brief Perform checks on the selected file name.
 * @details While we always set the file name in the INI (could run on different machines)
 * some integrity checks regarding existence, permissions, ..., are performed here.
 * @param[in] filename The chosen file name or path.
 */
void FilePath::checkValue(const QString &filename)
{
	info_text_->setVisible(true);
	const QString sub_path( pathsub(base_path_) );
	const QFileInfo file_info( sub_path + filename ); //file info object for filename + path + substitutions done
	path_text_->setToolTip(sub_path + filename);

	preview_button_->setEnabled(true);
	if (filename.isEmpty()) {
		setUpdatesEnabled(false);
		info_text_->setVisible(false);
		preview_button_->setEnabled(false);
	} else if (filename.trimmed().isEmpty()) {
		info_text_->setText(tr("[Empty file name]"));
	} else if (filename_only_ && base_path_.isNull()) { //no other checks possible, we don't know which path the file belongs to
		setUpdatesEnabled(false);
		info_text_->setVisible(false);
	} else if (io_mode == INPUT && !file_info.exists()) {
		info_text_->setText(path_only_? tr("[Folder does not exist]") : tr("[File does not exist]"));
	} else if (path_only_ && file_info.isFile()) {
		info_text_->setText(tr("[Directory path points to a file]"));
	} else if (!path_only_ && file_info.isDir()) {
		info_text_->setText(tr("[File path points to a directory]"));
	} else if (io_mode == INPUT && file_info.exists() && !file_info.isReadable()) {
		info_text_->setText(tr(
		    R"([File not readable for current user (owned by "%1")])").arg(file_info.owner()));
	} else if (io_mode == OUTPUT && file_info.exists() && !file_info.isWritable()) {
		info_text_->setText(tr(
		    R"([File not writable for current user (owned by "%1")])").arg(file_info.owner()));
	} else if (io_mode == UNSPECIFIED && file_info.exists() && !file_info.isReadable() && !file_info.isWritable()) {
		info_text_->setText(tr(
		    R"([File not accessible for current user (owned by "%1")])").arg(file_info.owner()));
	} else if (file_info.isExecutable() && !file_info.isDir()) {
		info_text_->setText(tr("[File is an executable]"));
	} else if (file_info.isSymLink()) {
		info_text_->setText(tr("[File is a symbolic link]"));
	} else if (io_mode == OUTPUT && !path_only_ && file_info.exists()) {
		info_text_->setText(tr("[File already exists]"));
	} else if (io_mode == OUTPUT && filename.trimmed() != filename) {
		info_text_->setText(tr("[File name has leading or trailing whitespaces]"));
	} else {
		setUpdatesEnabled(false);
		info_text_->setVisible(false);
	}

	setDefaultPanelStyles(filename);
	//the label is just info -> file does not actually have to exist and we set the value anyay:
	setIniValue(filename);
	setBufferedUpdatesEnabled(1); //hiding the info text sometimes flickers
}

/**
 * @brief Event listener for the link button.
 * @details That button is shown if the filename is linked to a path and is clickable to
 * highlight the corresponding widget.
 */
void FilePath::onLinkButtonClicked()
{
	if (link_button_->property("link_id").toString().isEmpty())
		return;
	const QStringList key_info( link_button_->property("link_id").toString().split("::") ); //presence of :: enforced by the regex
	getMainWindow()->getControlPanel()->getSectionTab()->showPanel(key_info.at(0), key_info.at(1));
}

/**
 * @brief Event listener for the open button.
 * @details Open a file or path by displaying a dialog window.
 */
void FilePath::openFile()
{
	path_text_->setProperty("shows_default", "true");
	QString start_path;
	if (base_path_.isEmpty())
		start_path = getSetting("auto::history::last_panel_path", "path");
	else
		start_path = pathsub(base_path_); //always start at base path if property "path" is set
	if (start_path.isEmpty())
		start_path = QDir::currentPath();

	QString path;
	if (path_only_) {
		path = QFileDialog::getExistingDirectory(this, tr("Open Folder"), start_path,
		    QFileDialog::ShowDirsOnly);
	} else {
		if (io_mode == INPUT) {
			path = QFileDialog::getOpenFileName(this, tr("Open File"), start_path,
				extensions_, nullptr, QFileDialog::DontConfirmOverwrite);
		} else if (io_mode == OUTPUT || io_mode == UNSPECIFIED) {
			path = QFileDialog::getSaveFileName(this, tr("Open File"), start_path,
			    extensions_, nullptr, QFileDialog::DontConfirmOverwrite);
		}
	}
	
	if (!path.isNull()) {
		if (base_path_.isEmpty()) { //a given base path takes precedence over last used
			const QFileInfo path_info( path + "/" );
			if (path_info.isDir())
				setSetting("auto::history::last_panel_path", "path", path_info.absolutePath());
			else
				setSetting("auto::history::last_panel_path", "path", path_info.absoluteDir().path());
		}

		if (filename_only_) {
			if (base_path_.isEmpty()) {
				checkValueAndSet(QFileInfo( path ).fileName());
			} else {
				const QDir dir( pathsub(base_path_) );
				const QString relative_path( dir.relativeFilePath(path) );
				checkValueAndSet( relative_path);
			}
		} else {
			checkValueAndSet(path);
		}
	} //endif !path.isNull
}

/**
 * @brief Event listener for the preview button.
 * @details Open a preview of the file.
 */
void FilePath::previewClicked()
{
	if (path_text_->text().isEmpty())
		return;
	auto *prev( new FilePreview(pathsub(base_path_) + path_text_->text(), getMainWindow()) );
	prev->show();
}

/**
 * @brief This function signals that the base path (set in a connected different widget) has changed.
 * @details If this panel has referenced a base path panel, then this slot is connected to the base
 * path panel's value being changed. I. e., if here we only display a file name (no path), and the said
 * path is being set in another FilePath panel with the "path=..." attribute, then the latter will fire
 * a signal to trigger this function and we can update here.
 * @param in_base_path Current path displayed in the base path FilePath panel.
 */
void FilePath::onBasePathChanged(const QString &in_base_path)
{
	base_path_ = in_base_path;
	checkValueAndSet( path_text_->text());
}
