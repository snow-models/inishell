//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2020 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Syntax highlighting for various file types (INI, XML, plain text, ...).
 * 2020-03
 */

#ifndef SYNTAXHIGHLIGHTER_H
#define SYNTAXHIGHLIGHTER_H

#include <QRegularExpression>
#include <QSyntaxHighlighter>
#include <QVector>
#include <QWidget>
#include <QtXml>

class SyntaxHighlighter : public QSyntaxHighlighter {
	Q_OBJECT

	public:
		SyntaxHighlighter(const QString &syntax = "ini", QTextDocument *textdoc = nullptr);

	protected:
		void highlightBlock(const QString &text) override;

	private:
		struct HighlightingRule {
			QRegularExpression pattern;
			QTextCharFormat format;
		};

		QVector<HighlightingRule> populateRules(const QString &syntax = "ini");
		QVector<HighlightingRule> populateIniRules();
		QVector<HighlightingRule> populateXmlRules();
		QVector<HighlightingRule> populatePlainRules();

		const QVector<HighlightingRule> rules_;
};

#endif //SYNTAXHIGHLIGHTER_H
