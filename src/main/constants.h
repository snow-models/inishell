//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2019 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * This file provides constants across INIshell. Most hardcoded values can be changed
 * from this central point.
 * 2019-10
 */

#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QString>

namespace Cst {

	/* Unicode character literals */
	static const QString u_star( QChar( 0x2217 ) ); //∗
	static const char32_t globe_32 = 0x1F30D; //outside QChar(char_16)
	static const QString u_globe( QString::fromUcs4(&globe_32, 1) ); //🌍
	static const QString u_warning( QChar( 0x26A0 ) ); //⚠
	static const QString u_valid( QChar( 0x2713 ) ); //✓
	static const QString u_link( QChar(0x29C9) ); //⧉ since U+1F517 is relatively new and not readily supported in fonts
	static const QString u_paragraph( QChar( 0x2029 ) ); //UNICODE paragraph separator

	/* main window */
	static constexpr int width_inishell_default = 1400;
	static constexpr int height_inishell_default = 800;
	static constexpr int width_inishell_min = 800; //window size limits
	static constexpr int height_inishell_min = 400;
	static constexpr int proportion_workflow_horizontal_percent = 20; //size of the left list
	static constexpr int width_workflow_max = 500;

	/* logger window */
	static constexpr int width_logger_default = 600;
	static constexpr int height_logger_default = 300;
	static constexpr int width_logger_min = 400;
	static constexpr int height_logger_min = 200;

	/* preview window */
	static constexpr int width_preview_default = 600;
	static constexpr int height_preview_default = 700;
	static constexpr int width_preview_min = 400;
	static constexpr int height_preview_min = 200;

	/* settings window */
	static constexpr int width_settings_default = 900;
	static constexpr int height_settings_default = 600;
	static constexpr int width_settings_min = 400;
	static constexpr int height_settings_min = 200;

	/* help windows */
	static constexpr int width_help_default = 1200;
	static constexpr int height_help_default = 600;
	static constexpr int width_help_min = 400;
	static constexpr int height_help_min = 200;
	static constexpr int width_help_about = 800;
	static constexpr int height_help_about = 600;

	/* file preview popup window */
	static constexpr int width_filepreview_default = 500;
	static constexpr int height_filepreview_default = 200;
	static constexpr int width_filepreview_min = 300;
	static constexpr int height_filepreview_min = 100;

	/* widgets */
	static constexpr int tiny = 100; //minimum width of (input) widgets, just to look less cramped
	static constexpr int width_button_std = 25; //standard width of push button
	static constexpr int height_button_std = 25;

	/* panels */
	static constexpr int nr_items_visible = 5; //CHECKLIST panel
	static constexpr int width_checklist_max = 300;
	static constexpr int checklist_safety_padding_vertical = 3;
	static constexpr int checklist_safety_padding_horizontal = 50; //space for checkboxes
	static constexpr int copytext_safety_padding = 12; //COPYTEXT panel
	static constexpr int width_copytext_min = 125;
	static constexpr int width_copytext_medium = 400;
	static constexpr int width_dropdown_max = 300; //DROPDOWN panel
	static constexpr int dropdown_safety_padding = 30;
	static constexpr int width_filepath_min = tiny * 2; //FILEPATH panel
	static constexpr int frame_left_margin = 20; //FRAME panel
	static constexpr int frame_right_margin = frame_left_margin;
	static constexpr int frame_top_margin = 25; //top is higher to account for caption
	static constexpr int frame_bottom_margin = 20;
	static constexpr int width_help = 600; //HELPTEXT panel
	static constexpr int width_label = 220; //LABEL panel
	static constexpr int width_long_label = 320; //to prettify lists of longer options
	static constexpr int label_padding = 20; //safety margins to really display all text
	static constexpr int width_number_min = 125; //NUMBER panel
	static constexpr int width_textbox_medium = 200; //TEXTFIELD panel
	static constexpr int default_spacer_size = 40; //SPACER panel

	/* workflow panel */
	static constexpr int treeview_indentation = 15;

	/* static strings */
	static const QString settings_file_name = "inishell_config.xml";
	static const QString sep = "::"; //do a grep if this is changed -- it's hardcoded in some messages
	static const QString section_open = "["; //check all RegularExpressions if this is changed
	static const QString section_close = "]";
	static const QString default_section("General");

	/* time */
	static constexpr int msg_length = 5000; //default ms for toolbar messages
	static constexpr int msg_short_length = 3000;

	/* popup preview settings */
	static constexpr int line_nr_normal = 200; //default number of lines to preview
	static constexpr int line_nr_more = line_nr_normal * 10;
	static constexpr int warn_file_size = 3e8; //warn if file is bigger than 300 MB

} //end namespace

#endif //CONSTANTS_H
