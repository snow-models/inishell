//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2020 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <src/main/os.h>

#include <QCoreApplication>
#include <QDir>
#include <QKeySequence>
#include <QPalette>
#include <QSettings>
#include <QStandardPaths>
#include <QtGlobal>
#include <QRegularExpression>

#include <iostream>

#if defined _WIN32 || defined __MINGW32__
	#ifndef NOMINMAX
		#define NOMINMAX
	#endif
	#include <windows.h>
	#include "Shlwapi.h" // what is this??
#endif


namespace os {

/**
 * @brief Resolve and simplify a path. Unfortunatelly, there is no guarantee for a proper handling of UTF encodings,
 * so only use this call to display nicer paths!
 * @param[in] in_path path to cleanup
 * @return Prettified path.
 */
QString prettyPath(const QString& in_path)
{
#if defined _WIN32 || defined __MINGW32__
	//if this would not suffice, see http://pdh11.blogspot.ch/2009/05/pathcanonicalize-versus-what-it-says-on.html
	std::wstring in_path_str( in_path.toStdWString() );
	wchar_t **ptr = nullptr;
	wchar_t *out_buff = (wchar_t*)calloc(MAX_PATH, sizeof(wchar_t));
	const DWORD status = GetFullPathName(in_path_str.c_str(), MAX_PATH, out_buff, ptr);
	if (status!=0 && status<=MAX_PATH) in_path_str = out_buff;
	free(out_buff);

	std::replace(in_path_str.begin(), in_path_str.end(), L'\\', L'/');
	return QString::fromStdWString( in_path_str );
#else //POSIX
    std::string in_path_str( in_path.toStdString() );
	std::replace(in_path_str.begin(), in_path_str.end(), '\\', '/');

	errno = 0;
	char *real_path = realpath(in_path_str.c_str(), nullptr); //POSIX 2008
	if (real_path != nullptr) {
		const QString tmp( real_path );
		free(real_path);
		return tmp;
	} else {
		std::cerr << "Path expansion of \'" << in_path_str << "\' failed. Reason:\t" << std::strerror(errno) << "\n";
		return QString::fromStdString( in_path_str ); //something failed in realpath, keep it as it is
	}
#endif
}

/**
 * @brief Check if we're running on KDE Desktop environment.
 * @details On a well-tuned KDE, things should be the smoothest.
 * @return True if on KDE.
 */
inline bool isKDE()
{
	const QString DE( QString::fromLocal8Bit(qgetenv("XDG_CURRENT_DESKTOP")) );
	return (DE == "KDE");
}

/**
 * @brief HACK: remove system-dependent keyboard shortcuts markers of a string
 * @details This is needed as a workaround for KDE bug https://bugs.kde.org/show_bug.cgi?id=337491
 * where keyboard shortcuts for tabs are created and included in the tab name.
 * @param[in] string String to remove keyboard shortcuts from.
 * @return The string with removed keyobard accelerators.
 */
QString cleanKDETabStr(const QString &string)
{
	if (isKDE()) {
		QString str_copy(string);
		return str_copy.replace("&", "");
	} else {
		return string;
	}
}

/**
 * @brief Looking at the default palette, return true if it is a dark theme
 * @return true if the current color theme is dark
 */
bool isDarkTheme()
{
#ifdef Q_OS_WIN
	QSettings settings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Themes\\Personalize", QSettings::NativeFormat);
	return (settings.value("AppsUseLightTheme")==0);
#else
	const int bg_lightness = QPalette().color(QPalette::Window).lightness();
	return (bg_lightness < 128);
#endif
}

/**
 * @brief Choose a number of locations to look for applications at.
 * @param[out] locations List of locations which will have the system specific ones appended.
 */
void getResourcesSystemLocations(QStringList &locations)
{
	const QString app_path( QCoreApplication::applicationDirPath() ); //directory that contains the application executable
	//redundant locations are kept in each #ifdef in order to control the priority of each path, per plateform

#if defined Q_OS_WIN
	locations << "../.."; //this is useful for some out of tree builds
	locations << app_path;
	locations << app_path + "/..";
	locations << app_path + "/../share";
	locations << QStandardPaths::standardLocations(QStandardPaths::DesktopLocation);
	locations << QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);
	locations << QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
	locations << QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation); //such as C:/ProgramData, C:/Users/<USER>/AppData/Local
	locations << QStandardPaths::standardLocations(QStandardPaths::AppDataLocation); //location where persistent application data can be stored
#endif
#if defined Q_OS_MAC
	locations << "./../../../.."; //this is useful for some out of tree builds: we must get out of the bundle
	locations << app_path;
	locations << app_path + "/..";
	locations << app_path + "/../share";
	locations << app_path + "/../../../.."; //we must get out of the bundle

	const QString app_real_path( prettyPath(app_path) );
	if (app_real_path!=app_path) {
		locations << app_real_path;
		locations << app_real_path + "/..";
		locations << app_real_path + "/../share";
		locations << app_real_path + "/../../../.."; //we must get out of the bundle
	}

	locations << QStandardPaths::standardLocations(QStandardPaths::DesktopLocation);
	locations << QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);
	locations << QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
	locations << QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation);
	locations << QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
	locations << QDir::homePath() + "/usr/share";
#endif
#if !defined Q_OS_WIN && !defined Q_OS_MAC
	locations << app_path;
	locations << app_path + "/..";
	locations << app_path + "/../share";

	const QString app_real_path( prettyPath(app_path) );
	if (app_real_path!=app_path) {
		locations << app_real_path;
		locations << app_real_path + "/..";
		locations << app_real_path + "/../share";
	}

	locations << QStandardPaths::standardLocations(QStandardPaths::DesktopLocation);
	locations << QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation); //$HOME/Documents
	locations << QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
	locations << QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation);
	locations << QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
	locations << QDir::homePath() + "/usr/share";
#endif
}

/**
 * @brief Extended search paths for executables, to preprend or append to a PATH variable.
 * @param[in] appname application's name.
 */
QString getBinExtraPath(const QString& appname)
{
	const QString own_path( QCoreApplication::applicationDirPath() ); //so exe copied next to inishell are found, this is usefull for packaging
	const QString home( QDir::homePath() );
	const QString desktop( QStandardPaths::standardLocations(QStandardPaths::DesktopLocation).at(0) ); //DesktopLocation always returns 1 element
	const QString documents( QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).at(0) );

#if defined Q_OS_WIN
	QString extra_path( ";" + desktop+"\\"+appname+"\\bin;" + home+"\\src\\"+appname+"\\bin;" + documents+"\\src\\"+appname+"\\bin;" + "D:\\src\\"+appname+"\\bin;" + "C:\\Program Files\\"+appname+"\\bin;" + "C:\\Program Files (x86)\\"+appname+"\\bin;" + own_path);

	const QString reg_key("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\" + appname + "\\UninstallString");
	QSettings settings;
	const QString uninstallExe( settings.value(reg_key).toString() );
	if (!uninstallExe.isEmpty()) {
		const QString installPath( QFileInfo( uninstallExe ).absolutePath() );
		if (!installPath.isEmpty())
			extra_path.append( ";"+installPath );
	}

	return extra_path;
#endif
#if defined Q_OS_MAC
	QString Appname( appname );
	Appname[0] = Appname[0].toUpper();

	const bool already_capitalized = (Appname[0]==appname[0]); //was the first letter of appname already capitalized?

	const QString user_paths( home+"/bin:" + home+"/usr/bin:" + home+"/src/"+appname+"/bin:" + documents+"/src/"+appname+"/bin:" + desktop+"/"+appname+"/bin" );
	const QString system_paths = (already_capitalized)? "/Applications/"+appname+".app/Contents/bin:" + "/Applications/"+appname+"/bin" : "/Applications/"+appname+".app/Contents/bin:" + "/Applications/"+Appname+".app/Contents/bin:" + "/Applications/"+appname+"/bin:" + "/Applications/"+Appname+"/bin";

	const QString extra_path( ":" +user_paths+ ":" +system_paths+ ":" + own_path + ":" + own_path+"/../../.."); //own_path + getting out of a .app
	return extra_path;
#endif
#if !defined Q_OS_WIN && !defined Q_OS_MAC
	const QString extra_path( ":" + home+"/bin:" + home+"/usr/bin:" + home+"/src/"+appname+"/bin:" + documents+"/src/"+appname+"/bin:" + desktop+"/"+appname+"/bin:" + "/opt/"+appname+"/bin:" + own_path );
	return extra_path;
#endif
}

/**
 * @brief Extend path where to search for executables, ie a proper initialization for a PATH variable.
 * @param[in] appname application's name.
 * @param[in] root_path original path that will be extended and exported to PATH (usually, original value of PATH)
 */
void setSystemPath(const QString& appname, QString root_path)
{
#if !defined Q_OS_WIN
	const QRegularExpression re(":~");
	root_path.replace(re,":"+ QDir::homePath());
#endif
	const QString env_path( root_path + getBinExtraPath(appname) );
	qputenv("PATH", env_path.toLocal8Bit());
}

/**
 * @brief Return native help keyboard shortcut as string to use in guidance texts.
 * @return Help key sequence as string.
 */
QString getHelpSequence() {
	const QKeySequence help_sequence(QKeySequence::HelpContents);
	return help_sequence.toString(QKeySequence::NativeText); //display as is on the machine
}

/**
 * @brief Return the user's logname of the computer running the current process.
 * @return current username or empty string
 */
QString getLogName()
{
	char *tmp;

	if ((tmp=getenv("USERNAME"))==nullptr) { //Windows & Unix
		if ((tmp=getenv("LOGNAME"))==nullptr) { //Unix
			tmp=getenv("USER"); //Windows & Unix
		}
	}

	if (tmp==nullptr) return QString();
	return QString(tmp);
}


// custom implementation of readDirectories and file exists
void readDirectory(const std::string& path, QStringList& dirlist, const std::vector<std::string>& pattern, const bool& isRecursive, std::unordered_set<std::string>& visited_dirs)
{
	readDirectoryPrivate(path, dirlist, pattern, isRecursive, visited_dirs);
}

QStringList readDirectory(const std::string& path, const std::vector<std::string>& pattern, const bool& isRecursive, std::unordered_set<std::string>& visited_dirs)
{
	QStringList dirlist;
	readDirectoryPrivate(path, dirlist, pattern, isRecursive, visited_dirs);
	return dirlist;
}

#if defined _WIN32 || defined __MINGW32__
bool doesDirectoryExist(const std::string& path) {
    DWORD dwAttrib = GetFileAttributesA(path.c_str());

    return (dwAttrib != INVALID_FILE_ATTRIBUTES && 
         (dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

void readDirectoryPrivate(const std::string& path, QStringList& dirlist, const std::vector<std::string>& pattern_vec, const bool& isRecursive, std::unordered_set<std::string>& visited_dirs, int max_depth) {
	if (max_depth > MAX_RECURSION_DEPTH) return;
	const size_t path_length = path.length();
	if (path_length > (MAX_PATH - 1)) {
		std::cerr << "Path " << path << "is too long (" << path_length << " characters)" << std::endl;
		return;
	}
	WIN32_FIND_DATAA findFileData;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	std::string fullpath(path+"\\\\*");
	hFind = FindFirstFileA(fullpath.c_str(), &findFileData);
    if (hFind == INVALID_HANDLE_VALUE) {
        return;
    } else {
        do {
            const std::string filename(findFileData.cFileName);
            if (filename == "." || filename == "..") continue;
            std::string full_path = path + "\\" + filename;
			if (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				if (isRecursive) {
					readDirectoryPrivate(full_path, dirlist, pattern_vec, isRecursive, visited_dirs, max_depth+1);
				}
			} else {
				if (!(findFileData.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)) {
					for (const std::string& pattern : pattern_vec) {
						const size_t pos = filename.find(pattern);
						if (pos != std::string::npos) {
							dirlist.push_back(QString::fromStdString(full_path));
						}
					}
				}
			}
        } while (FindNextFileA(hFind, &findFileData) != 0);

		const DWORD dwError( GetLastError() );
		if (dwError != ERROR_NO_MORE_FILES) {
			std::cerr << "Error reading directory: " << dwError << std::endl;
			return;
		}

        FindClose(hFind);
    }
}
#else
bool doesDirectoryExist(const std::string& path) {
    struct stat info;

    if(stat(path.c_str(), &info) != 0) {
        return false; // Cannot access the path
    } else if(info.st_mode & S_IFDIR) {
        return true; // This is a directory
    } else {
        return false; // This is not a directory
    }
}


//sub_path contains the relative path that should prefix the found file names (this is only useful for recursive search)
void readDirectoryPrivate(const std::string& path, QStringList& dirlist, const std::vector<std::string>& pattern_vec, const bool& isRecursive, std::unordered_set<std::string>& visited_dirs, int max_depth)
{
	if (max_depth > MAX_RECURSION_DEPTH) return;
	if (!doesDirectoryExist(path)) return;
	if (visited_dirs.find(path) != visited_dirs.end()) return; //skip already visited directories
	DIR *dp = opendir(path.c_str());
	if (dp == nullptr) {
		return;
	}

	struct dirent *dirp;
	while ((dirp = readdir(dp)) != nullptr) {
		const std::string filename( dirp->d_name );
		if ( filename.compare(".")==0 || filename.compare("..")==0 )
			continue; //skip "." and ".."
		
		const std::string full_path( path+"/"+filename );
		struct stat statbuf;
		if (stat(full_path.c_str(), &statbuf) == -1) {
			if (lstat(full_path.c_str(), &statbuf) != -1) continue;
			else continue;
		}
	#if defined HAVE_STRUCT_STAT_ST_FLAGS
		const bool hidden_flag = (filename.compare(0,1,".")==0) || (statbuf.st_flags & UF_HIDDEN); //for osX and BSD
	#else
		const bool hidden_flag = (filename.compare(0,1,".")==0);
	#endif
		if (hidden_flag) continue; //skip hidden files/directories
		
		if (!isRecursive) {
			if (pattern_vec.empty()) {
				dirlist.push_back( QString::fromStdString(full_path) );
			} else {
				for (const std::string& pattern : pattern_vec) {
					const size_t pos = filename.find( pattern );
					if (pos!=std::string::npos) {
						dirlist.push_back( QString::fromStdString(full_path) );
						break;
					}
				}
			}
		} else {
			if (S_ISDIR(statbuf.st_mode)) { //recurse on sub-directory
				readDirectoryPrivate(full_path, dirlist, pattern_vec, isRecursive, visited_dirs, max_depth+1);
			} else {
				if (!S_ISREG(statbuf.st_mode)) continue; //skip non-regular files, knowing that "stat" already resolved the links
				if (pattern_vec.empty()) {
					dirlist.push_back( QString::fromStdString(full_path) );
				} else {
					for (const std::string& pattern : pattern_vec) {
						const size_t pos = filename.find(pattern);
						if (pos!=std::string::npos) {
							dirlist.push_back( QString::fromStdString(full_path) );
							break;
						}
					}
				}
			}
		}
	}
	closedir(dp);
	visited_dirs.insert(path);
}
#endif

std::string cdUp(std::string path, const short int& nrUp)
{
	short int count=0;

	while (count < nrUp) {
		if (path.empty()) return path;

		if (path.back() == '/' || path.back() == '\\') path.pop_back();
		const size_t pos = path.find_last_of("/\\");
		if (pos == 0 || pos == std::string::npos) return path;
		path.erase(pos, std::string::npos);
		count++;
	}

	return path;
}

} //namespace os
