//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2019 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <src/main/colors.h>
#include <src/main/common.h>
#include <src/main/settings.h>
#include <src/main/os.h>

#include <QDir>
#include <QFileInfo>
#include <QProcessEnvironment>

#include <vector>



namespace html {

/**
 * @brief Add HTML embolden tags to text.
 * @param[in] text Text to add embolden tags to.
 * @return Text enclosed by embolden tags.
 */
QString bold(const QString &text)
{
	return "<b>" + text + "</b>";
}

/**
 * @brief Add HTML emphasizing tags to text.
 * @param[in] text Text to emphasize.
 * @return Text enclosed by emphasizing tags tags.
 */
QString emph(const QString &text)
{
	return "<em>" + text + "</em>";
}

/**
 * @brief Add HTML color tags to text.
 * @param[in] text Text to add color tags to.
 * @return Text enclosed by color tags.
 */
QString color(const QString &text, const QString &color)
{
	const QString strRet( "<font color=\"" + colors::getQColor(color).name() + "\">" + text + "</font>");
	return strRet;
}

} //namespace html

QIcon getIcon(const QString& icon_name)
{
	const bool use_darkmode = colors::useDarkTheme(); //decide whether to use dark mode
	static const QString fallbackPath = (use_darkmode)? ":/icons/flat-bw-dark/svg/" : ":/icons/flat-bw-light/svg/";
#ifdef Q_OS_WIN
	return QIcon(fallbackPath+icon_name+".svg");
#else
	#ifdef Q_OS_MAC
		return QIcon(fallbackPath+icon_name+".svg");
	#else //on Linux, BSD..., try to use system icons
		return QIcon::fromTheme(icon_name, QIcon(fallbackPath+icon_name+".svg"));
	#endif
#endif
}

#if defined Q_OS_MAC
/**
 * @brief Extended search paths for resources, to preprend or append (case insensitive) to a search path variable on Mac.
 * @param[in] appname application's name.
 */
QString getExtraResourcePathCIMAC(const QString& appname)
{
	const QString own_path( QCoreApplication::applicationDirPath() ); //so exe copied next to inishell are found, this is usefull for packaging
	const QString home( QDir::homePath() );
	const QString desktop( QStandardPaths::standardLocations(QStandardPaths::DesktopLocation).at(0) ); //DesktopLocation always returns 1 element
	const QString documents( QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).at(0) );

	const QString user_paths( home+"/src/"+appname + ":" + documents+"/src/"+appname + ":" + desktop+"/"+appname );
	const QString system_paths = "/Applications/"+appname+".app/Contents/Resources:" + ":" + "/Applications/"+appname;

	const std::string outOfBundle( os::cdUp(own_path.toStdString(), 4) );
	const QString extra_path( user_paths+ ":" +system_paths+ ":" +own_path+":"+QString::fromStdString(outOfBundle) ); //own_path + getting out of a .app

	//non-existent paths and redundant paths will be filtered by the caller (see getSearchDirs())
	return extra_path;
}
#endif

/**
 * @brief Fill a list with directories to search for XMLs.
 * @details This function queries a couple of default folders on various systems, as well as
 * ones that can be set by the user. Duplicates (e. g. the same folder given by a relative
 * and an absolute path) are ignored.
 * @return A list of directories to search for XMLs.
 */
QStringList getSearchDirs(const bool &include_user_set, const bool &include_nonexistent_folders, const QString& own_path)
{
	// hardcoded directories, the system specific paths are partly handled by Qt
	QStringList locations;
	locations << "."; //the application's current directory
	os::getResourcesSystemLocations(locations); //update list with OS specific search paths

	//TODO: recursive search
	QStringList dirs;
	for (const auto &tmp_dir : locations) {
		dirs << tmp_dir + "/inishell-apps";
		dirs << tmp_dir + "/simulations";
	}

	//add paths to apps, for when the recommended folder structure is followed
	//const QString own_path( QCoreApplication::applicationDirPath() ); //so things copied next to inishell are found, this is usefull for packaging
	dirs << own_path + "/../../meteoio/inishell-apps";
	dirs << own_path + "/../meteoio/inishell-apps";
	dirs << own_path + "/../../snowpack/inishell-apps";
	dirs << own_path + "/../snowpack/inishell-apps";
	dirs << own_path + "/../../alpine3d/inishell-apps";
	dirs << own_path + "/../alpine3d/inishell-apps";
#ifdef Q_OS_MAC
	const std::string tmp_path_mac(os::cdUp(own_path.toStdString(), 4)); //get out of the bundle
	dirs << QString::fromStdString(tmp_path_mac+"/meteoio/inishell-apps");
	dirs << QString::fromStdString(tmp_path_mac+"/snowpack/inishell-apps");
	dirs << QString::fromStdString(tmp_path_mac+"/alpine3d/inishell-apps");
#endif

	// find apps from environment variables
	QString value_PATH( QString::fromLocal8Bit(qgetenv("PATH")) ); //original PATH content with platform specific additions
#if !defined Q_OS_WIN
	const QRegularExpression re(":~");
	value_PATH.replace(re,":"+ QDir::homePath());
#endif
#if defined Q_OS_MAC
	//HACK still hardcoded application names, couldnt find a way to reliably read application names globally
	for (const QString &appname : getApplicationNames())
		value_PATH += ":"+getExtraResourcePathCIMAC(appname);
#endif
	if (!value_PATH.isEmpty()){
		const QStringList path_entries( QString(value_PATH).split(":") );
		for (const auto &tmp_dir : path_entries){
			dirs << tmp_dir + "/../share";
			dirs << tmp_dir + "/../inishell-apps";
			dirs << tmp_dir + "/../simulations";
			dirs << tmp_dir;
		}
	}

#if defined Q_OS_MAC
	QString value_LD_LIB( QString::fromLocal8Bit(qgetenv("DYLD_FALLBACK_LIBRARY_PATH")) );
#else
	QString value_LD_LIB( QString::fromLocal8Bit(qgetenv("LD_LIBRARY_PATH")) );
#endif
#if !defined Q_OS_WIN
	value_LD_LIB.replace(re,":"+ QDir::homePath());
#endif	
	if (!value_LD_LIB.isEmpty()){
		const QStringList ld_lib_entries( QString(value_LD_LIB).split(":") );
		for (const auto &tmp_dir : ld_lib_entries){
			dirs << tmp_dir + "/../share/inishell-apps";
			dirs << tmp_dir + "/share/inishell-apps";
		}
	}

	if (include_user_set) { // include user set paths
		const QStringList user_xml_paths( getListSetting("user::xmlpaths", "path") );
		dirs.append(user_xml_paths);
	}
	
	//now, check that we don't have the same folder multiple times (e. g. via relative and absolute paths) and remove non-existent paths
	QStringList filtered_dirs;
	for (const auto &dir : dirs) {
		QDir tmp( dir );
		if (!include_nonexistent_folders && !tmp.exists()) continue;
		const QString tmp_canonical( tmp.canonicalPath() );
		bool found = false;
		for (const auto &filtered_dir : filtered_dirs) {
			if (tmp_canonical == filtered_dir) {
				found = true;
				break;
			}
		}
		if (!found)
			filtered_dirs << tmp_canonical;
	}
	
	return filtered_dirs;
}

/**
 * @brief Convert a key press event to a key sequence.
 * @details This way key sequences can also be used in key press event listeners.
 * @param[in] event Input key press event.
 * @return The key event converted to a QKeySequence.
 */
QKeySequence keyToSequence(QKeyEvent *event)
{ //this is a bit of a HACK: get key sequences in event listeners
	QString modifier;
	if (event->modifiers() & Qt::ShiftModifier)
		modifier += "Shift+";
	if (event->modifiers() & Qt::ControlModifier)
		modifier += "Ctrl+";
	if (event->modifiers() & Qt::AltModifier)
		modifier += "Alt+";
	if (event->modifiers() & Qt::MetaModifier)
		modifier += "Meta+";

	const QString key_string( QKeySequence(event->key()).toString() );
	return QKeySequence( modifier + key_string );
} //https://forum.qt.io/topic/73408/qt-reading-key-sequences-from-key-event

// #endif
