//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2020 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Operating system specific functionalities.
 * 2020-03
 */

#ifndef OS_H
#define OS_H

#include <QString>
#include <QStringList>

#include <iostream> // TODO: remove

#include <exception>
#include <list>
#include <unordered_set>

#if defined _WIN32 || defined __MINGW32__
	#ifndef NOMINMAX
		#define NOMINMAX
   #endif
#else
	#include <dirent.h>
	#include <unistd.h>
	#include <cstring>
	#include <iterator>
	#include <string>
	#include <sys/stat.h>

#endif

namespace os {

static const int MAX_RECURSION_DEPTH = 2;

void getResourcesSystemLocations(QStringList &locations);
QString prettyPath(const QString& in_path);
bool isKde();
QString cleanKDETabStr(const QString &string);
bool isDarkTheme();
QString getBinExtraPath(const QString& appname);
void setSystemPath(const QString& appname, QString root_path);
QString getHelpSequence();
QString getLogName();
bool doesDirectoryExist(const std::string& path);

void readDirectoryPrivate(const std::string& path, QStringList& dirlist, const std::vector<std::string>& pattern_vec, const bool& isRecursive, std::unordered_set<std::string>& visited_dirs, int max_depth=1);

/**
* @brief Build a list of file in a given directory.
* @details
* The matching is very primitive: it only looks for the substring "pattern" in the file names.
* If this substrings exists, the file matches. In the case of recursive search, the filenames will be
* prefixed by their relative path based on the provided path.
* @note Hidden files/directories are excluded, links are followed
* @param path directory containing the files
* @param dirlist list of matching file names
* @param pattern optional pattern that must be part of the file names
* @param isRecursive should the search recurse through sub-directories? (default: false)
*/
void readDirectory(const std::string& path, QStringList& dirlist, const std::vector<std::string>& pattern, const bool& isRecursive, std::unordered_set<std::string>& visited_dirs);

QStringList readDirectory(const std::string& path, const std::vector<std::string>& pattern, const bool& isRecursive, std::unordered_set<std::string>& visited_dirs);

std::string cdUp(std::string path, const short int& nrUp=1);

} //endif namespace

#endif // OS_H
