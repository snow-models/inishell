//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2020 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <src/main/SyntaxHighlighter.h>

#include <src/main/colors.h>
#include <src/main/constants.h>
#include <src/main/inishell.h>
#include <src/panels/Atomic.h>

/**
 * @class SyntaxHighlighter
 * @brief Default constructor for a syntax highlighter.
 * @details This could be the preview editor (INI syntax highlighting), help documents (XML highlighting),
 * plain text (file input widget), ...
 * @param[in] textdoc Text document to handle syntax highlighting for.
 */
SyntaxHighlighter::SyntaxHighlighter(const QString &syntax, QTextDocument *textdoc) : QSyntaxHighlighter(textdoc), rules_(populateRules(syntax)) {}

/**
 * @brief Add highlighting rules according to text content.
 * @param syntax The content type ("ini", "xml", "plain", ...)
 * @return the matching highlighting rules
 */
QVector<SyntaxHighlighter::HighlightingRule> SyntaxHighlighter::populateRules(const QString &syntax)
{
	if (syntax.toLower() == "ini") {
		return populateIniRules();
	} else if (syntax.toLower() == "xml") {
		return populateXmlRules();
	} else if (syntax.toLower() == "plain") {
		return populatePlainRules();
	}

	return QVector<HighlightingRule>();
}

/**
 * @brief Send a chunk of text through the syntax highlighter.
 * @param[in] text The text to syntax-highlight.
 */
void SyntaxHighlighter::highlightBlock(const QString &text)
{
	for (const HighlightingRule &rule : rules_) {
		QRegularExpressionMatchIterator mit( rule.pattern.globalMatch(text) );
		while (mit.hasNext()) { //run trough regex matches and set the stored formats
			QRegularExpressionMatch match( mit.next() );
			setFormat(static_cast<int>(match.capturedStart()), static_cast<int>(match.capturedLength()), rule.format);
		}
	}
}

/**
 * @brief Construct highlighting rules for INI files.
 * @return the matching highlighting rules
 */
QVector<SyntaxHighlighter::HighlightingRule> SyntaxHighlighter::populateIniRules()
{
	QVector<HighlightingRule> vec_rules;
	HighlightingRule rule;

	/*
	 * The SyntaxHighlighter parses a document for INI file syntax and checks the sections
	 * and keys against the curently loaded XML. Sections and keys that are not present
	 * in the XML are highlighted differently.
	 */

	/* unknown sections */
	QTextCharFormat format_section;
	format_section.setForeground(colors::getQColor("syntax_unknown_section"));
	format_section.setFontWeight(QFont::Bold);
	rule.pattern = QRegularExpression(R"(.*\)" + Cst::section_open + R"(.*\)" + Cst::section_close + R"(.*)");
	rule.format = format_section;
	vec_rules.append(rule);

	/* unknown keys */
	QTextCharFormat format_unknown_key;
	format_unknown_key.setForeground(colors::getQColor("syntax_unknown_key"));
	rule.pattern = QRegularExpression(R"(^\s*([\w|*]+(::)?[\w|*]*)+(?=\s*=))"); //only :: but not :
	//TODO: collect and unify all regex handling (e. g. use the same here as in INIParser)
	rule.format = format_unknown_key;
	vec_rules.append(rule);

	/* INI values */
	QTextCharFormat format_value;
	format_value.setForeground(colors::getQColor("syntax_value"));
	rule.pattern = QRegularExpression(R"((?<=\=).*)");
	rule.format = format_value;
	vec_rules.append(rule);

	/* populate highlighter with known sections and keys */
	QTextCharFormat format_known_key;
	format_known_key.setForeground(colors::getQColor("syntax_known_key"));
	QTextCharFormat format_known_section;
	format_known_section.setForeground(colors::getQColor("syntax_known_section"));
	format_known_section.setFontWeight(QFont::Bold);

	QStringList added_sections_; //to avoid duplicated rules
	const QList<Atomic *> panel_list( getMainWindow()->findChildren<Atomic *>() );
	for (auto &panel : panel_list) {
		if (panel->property("no_ini").toBool()) //e. g. Groups / frames
			continue;
		QString section, key;
		(void) panel->getIniValue(section, key);
		key.replace("*", "\\*");
		rule.pattern = QRegularExpression("\\" + Cst::section_open + section + "\\" +
			Cst::section_close, QRegularExpression::CaseInsensitiveOption); //TODO: escape only if needed for the set char
		rule.format = format_known_section;
		if (added_sections_.indexOf(QRegularExpression(section, QRegularExpression::CaseInsensitiveOption)) == -1) {
			vec_rules.append(rule);
			added_sections_.append(section);
		}
		rule.pattern = QRegularExpression(R"(^\s*)" + key + R"((=|\s))", QRegularExpression::CaseInsensitiveOption);
		rule.format = format_known_key;
		vec_rules.append(rule);
	}
	//for dynamic sections we do not need to iterate through the GUI because we have them in a list:
	QStringList *dynamic_sections( getMainWindow()->getControlPanel()->getSectionTab()->getDynamicSectionList() );
	for (auto &dyn_sec : *dynamic_sections) { //add all dynamic sections with numeric postfixes
		rule.pattern = QRegularExpression("\\" + Cst::section_open + dyn_sec + "[0-9]+" + "\\" +
			Cst::section_close, QRegularExpression::CaseInsensitiveOption);
		rule.format = format_known_section;
		vec_rules.append(rule);
	}

	/* comments */
	QTextCharFormat format_block_comment;
	format_block_comment.setForeground(colors::getQColor("syntax_comment"));
	rule.pattern = QRegularExpression(R"(^\s*[#;].*)");
	rule.format = format_block_comment;
	vec_rules.append(rule);
	rule.pattern = QRegularExpression(R"(([#;](?!$).*))");
	vec_rules.append(rule);

	/* coordinates */
	QTextCharFormat format_coordinate;
	format_coordinate.setForeground(colors::getQColor("coordinate"));
	rule.pattern = QRegularExpression(R"((latlon|xy)\s*\(([-\d\.]+)(?:,)\s*([-\d\.]+)((?:,)\s*([-\d\.]+))?\))");
	rule.format = format_coordinate;
	vec_rules.append(rule);

	/* equals sign */
	QTextCharFormat format_equal;
	format_equal.setForeground(Qt::black);
	rule.pattern = QRegularExpression(R"(=)");
	rule.format = format_equal;
	vec_rules.append(rule);

	return vec_rules;
}

/**
 * @brief Construct highlighting rules for XML files.
 * @return the matching highlighting rules
 */
QVector<SyntaxHighlighter::HighlightingRule> SyntaxHighlighter::populateXmlRules()
{
	QVector<HighlightingRule> vec_rules;
	HighlightingRule rule;

	/* XML syntax highlighting */
	QTextCharFormat format_element; //XML elements
	format_element.setForeground(colors::getQColor("syntax_xml_element"));
	format_element.setFontWeight(QFont::Bold);
	rule.pattern = QRegularExpression("<[?\\s]*[/]?[\\s]*([^\\n][^>]*)(?=[\\s/>])");
	rule.format = format_element;
	vec_rules.append(rule);

	QTextCharFormat format_attribute; //XML attributes
	format_attribute.setForeground(colors::getQColor("syntax_xml_attribute"));
	format_attribute.setFontWeight(QFont::Bold);
	format_attribute.setFontItalic(true);
	rule.pattern = QRegularExpression("\\w+(?=\\=)");
	rule.format = format_attribute;
	vec_rules.append(rule);

	QTextCharFormat format_value; //XML values
	format_value.setForeground(colors::getQColor("syntax_xml_value"));
	rule.pattern = QRegularExpression("\"[^\\n\"]+\"(?=[?\\s/>])");
	rule.format = format_value;
	vec_rules.append(rule);

	QTextCharFormat format_comment; //XML comments
	format_comment.setForeground(colors::getQColor("syntax_xml_comment"));
	rule.pattern = QRegularExpression("<!--[^\\n]*-->");
	rule.format = format_comment;
	vec_rules.append(rule);

	QTextCharFormat format_keyword; //XML keywords
	format_keyword.setForeground(colors::getQColor("syntax_xml_keyword"));
	format_keyword.setFontWeight(QFont::Bold);
	rule.format = format_keyword;
	QStringList keyword_patterns;
	keyword_patterns << "<\\?" << "/>" << ">" << "<" << "</" << "\\?>";
	for (auto &pat : keyword_patterns) {
		rule.pattern = QRegularExpression( pat );
		vec_rules.append(rule);
	}

	return vec_rules;
}

/**
 * @brief Construct highlighting rules for plain text files.
 * @return the matching highlighting rules
 */
QVector<SyntaxHighlighter::HighlightingRule> SyntaxHighlighter::populatePlainRules()
{
	QVector<HighlightingRule> vec_rules;
	HighlightingRule rule;

	/*
	 * The goal of the mode is to help figure out what the syntax of a plain text file is
	*/

	QTextCharFormat format_text; //pure alphabetical characters, most probably comments
	format_text.setForeground( colors::getQColor("helptext") );
	rule.pattern = QRegularExpression(R"((\p{L}+|\p{M}+|[0-9])+)");
	rule.format = format_text;
	vec_rules.append(rule);

	QTextCharFormat format_nulls; //NULL, NAN, etc
	format_nulls.setForeground( colors::getQColor("sl_green") );
	rule.pattern = QRegularExpression(R"((?i)(\+|\-)?(nan|null|none|1\.#snan|1\.#qnan|1\.#ind|nanq|nans|nan%|qnan|snan|undef|nodata))");
	rule.format = format_nulls;
	vec_rules.append(rule);

	QTextCharFormat format_numbers; //numbers, including in scientific notation, assuming '.' as decimal separator
	format_numbers.setForeground( colors::getQColor("number") );
	rule.pattern = QRegularExpression(R"((\+|\-)?[[:digit:]](\.[[:digit:]]+)?((e|E)(\+|\-)?[[:digit:]]+)?)");
	rule.format = format_numbers;
	vec_rules.append(rule);

	QTextCharFormat format_wsp; //white spaces
	format_wsp.setForeground( colors::getQColor("special") );
	rule.pattern = QRegularExpression(R"([[:space:]])");
	rule.format = format_wsp;
	vec_rules.append(rule);

	QTextCharFormat format_date; //several known representations of date
	format_date.setForeground( colors::getQColor("sl_yellow") );
	rule.pattern = QRegularExpression(R"([0-9]{2,4}(\.|\-)[0-9]{2,4}(\.|\-)[0-9]{2,4})");
	rule.format = format_date;
	vec_rules.append(rule);

	QTextCharFormat format_time; //several known representations of time
	format_time.setForeground( colors::getQColor("sl_yellow") );
	rule.pattern = QRegularExpression(R"([0-9]{2}\:[0-9]{2}(\:[0-9]{2})?((am)|(pm))?)");
	rule.format = format_time;
	vec_rules.append(rule);

	QTextCharFormat format_cntrl; //control characters (non-printable)
	format_cntrl.setForeground( colors::getQColor("warning") );
	rule.pattern = QRegularExpression(R"([[:cntrl:]])");
	rule.format = format_cntrl;
	vec_rules.append(rule);

	QTextCharFormat format_sections; //"[section]" kind of syntax
	format_sections.setForeground( colors::getQColor("syntax_known_section") );
	rule.pattern = QRegularExpression(R"(^\[(\p{L}+|\p{M}+|[0-9])+\]\w*$)");
	rule.format = format_sections;
	vec_rules.append(rule);

	QTextCharFormat format_info; //status messages not part of the file itself
	format_cntrl.setForeground( colors::getQColor("warning") );
	rule.pattern = QRegularExpression(tr("(truncated - right-click to show more)")); //very bad luck if this is actually contained in the file...
	rule.format = format_info;
	vec_rules.append(rule);

	return vec_rules;
}
