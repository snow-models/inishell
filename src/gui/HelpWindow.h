//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2021 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Window to display the INIshell help XMLs (user guide and developer help).
 * 2021-01
 */

#ifndef HELPWINDOW_H
#define HELPWINDOW_H

#include <QMainWindow>
#include <QString>

#include <src/gui/MainPanel.h>
#include <src/gui/SectionTab.h>
#include <src/panels/Group.h>

class HelpWindow : public QMainWindow {
	Q_OBJECT

	public:
		explicit HelpWindow(QMainWindow *parent = nullptr);
		void loadHelp(const bool &dev = false);
		SectionTab * getSectionTab() const noexcept { return help_tab_; }

	protected:
		void keyPressEvent(QKeyEvent *event) override;

	private:
		void createMenu();
		void helpAbout();

		SectionTab *help_tab_ = nullptr;
		QAction *view_user_guide_ = nullptr;
		QAction *view_dev_help_ = nullptr;
};

#endif //HELPWINDOW_H
