//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2021 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Add/remove button or button set for dynamic sections (to be displayed next to the tab names).
 * 2021-01
 */

#ifndef SECTIONBUTTON_H
#define SECTIONBUTTON_H

#include <QWidget>

class SectionButton : public QWidget {
	Q_OBJECT

	public:
		enum button_mode {
			plus,
			minus
		};
		explicit SectionButton(const button_mode &mode, const QString &section, QWidget *parent = nullptr);

	private slots:
		void onPlusClicked(const QString &section);
		void onMinusClicked(const QString &section);

};

#endif //SECTIONBUTTON_H
