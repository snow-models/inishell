//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2021 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <src/gui/SectionButton.h>
#include <src/main/inishell.h>

#include <QHBoxLayout>
#include <QToolButton>

#ifdef DEBUG
	#include <iostream>
#endif

/**
 * @class SectionButton
 * @brief A class to display properly sized +/- buttons in section tabs.
 * @param[in] mode Plus or minus button?
 * @param[in] section The section's name.
 * @param[in] parent The parent widget.
 */
SectionButton::SectionButton(const button_mode &mode, const QString &section, QWidget *parent) : QWidget(parent) {
	const QSize sz( 20, 20 ); //w, h
	const int gap_after_label = 5;
	this->setFixedWidth(sz.width() + gap_after_label);
	this->setFixedHeight(sz.height());

	auto button( new QToolButton );
	button->setAutoRaise(true);
	button->setFixedSize(sz);
	//button->setIconSize(sz);

	if (mode == plus) {
		connect(button, &QToolButton::clicked, this, [=] { onPlusClicked(section); });
		button->setIcon(getIcon("tab-new"));
		button->setToolTip(tr("Click to add another instance of this section to the INI file"));
	} else if (mode == minus) {
		button->setIcon(getIcon("tab-close"));
		connect(button, &QToolButton::clicked, this, [=] { onMinusClicked(section); });
		button->setToolTip(tr("Click to remove this section instance from the INI file"));
	}

	auto button_layout = new QHBoxLayout;
	button_layout->setContentsMargins(0, 0, 0, 0);
	button_layout->setSpacing(0);
	button_layout->addSpacerItem(new QSpacerItem(gap_after_label, sz.height(), QSizePolicy::Fixed));
	button_layout->addWidget(button, 0, Qt::AlignLeft);
	this->setLayout(button_layout);
}

/**
 * @brief Spawn a new dynamic section.
 * @details Gets called by a lambda on "+" button clicks.
 * @param[in] section The parent section to clone.
 */
void SectionButton::onPlusClicked(const QString &section) {
	getMainWindow()->getControlPanel()->getSectionTab()->spawnDynamicSection(section);
}

/**
 * @brief Remove a dynamic section.
 * @details Gets called by a lambda on "-" button clicks.
 * @param[in] section The child section to remove.
 */
void SectionButton::onMinusClicked(const QString &section) {
	getMainWindow()->getControlPanel()->getSectionTab()->removeDynamicSection(section);
}
