//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2019 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * The main window that is displayed on startup.
 * 2019-10
 */

#include <src/gui/MainWindow.h>
#include <src/gui/RememberDialog.h>
#include <src/gui/AboutWindow.h>
#include <src/main/colors.h>
#include <src/main/common.h>
#include <src/main/constants.h>
#include <src/main/Error.h>
#include <src/main/dimensions.h>
#include <src/main/INIParser.h>
#include <src/main/inishell.h>
#include <src/main/settings.h>
#include <src/panels/Atomic.h>
#include <src/panels/dynamic_panels.h>
#include <src/panels/Group.h> //to exclude Groups from panel search

#include <QApplication>
#include <QCheckBox>
#include <QDesktopServices>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QGroupBox>
#include <QMenuBar>
#include <QMessageBox>
#include <QProcess>
#include <QRegularExpression>
#include <QSpacerItem>
#include <QStatusBar>
#include <QSysInfo>
#include <QTimer>
#include <QToolBar>

#include <unistd.h>
#include <clocale>

#ifdef DEBUG
	#include <iostream>
#endif //DEBUG
#ifdef DEBUG
	#include <QElapsedTimer>
#endif //def DEBUG

/**
 * @class MouseEventFilter
 * @brief Mouse event listener for the main program window.
 * @details Needs to be implemented with installEventFilter() for each widget that is 
 * supposed to  be filtered
 * @param[in] object Object the event stems from.
 * @param[in] event The type of event.
 * @return True if the event was accepted.
 */
bool MouseEventFilter::eventFilter(QObject *object, QEvent *event)
{
	if (event->type() == QEvent::MouseButtonPress) {
		if (object->property("mouseclick") == "open_log") {
			getMainWindow()->viewLogger();
		} else if (object->property("mouseclick") == "open_ini") {
			if (!getMainWindow()->ini_filename_->text().isEmpty())
				QDesktopServices::openUrl("file:" + getMainWindow()->ini_filename_->text());
		}
	}
	return QObject::eventFilter(object, event); //pass to actual event of the object
}

/**
 * @class MainWindow
 * @brief Constructor for the main window.
 * @param[in] settings_location Path to the INIshell settings file.
 * @param[in] errors List of errors that have occurred before starting the GUI.
 * @param[in] parent Unused since the main window is kept in scope by the entry point function.
 */
MainWindow::MainWindow(QString &settings_location, const QStringList &errors, QMainWindow *parent)
    : QMainWindow(parent), logger_(this), xml_settings_filename_(settings_location)
{
	/*
	 * Program flowchart sketch:
	 *
	 *   Set application style and properties  <----- main.cc ----->  cmd line options,
	 *                  |                                             translations
	 *         Load INIshell settings  <----- main.cc ----->  store in global XML node
	 *                  |
	 *             Launch GUI  <----- MainWindow.cc's constructor
	 *                  |
	 *  User opens:     v
	 *      application or simulation                   INI file  <----- INIParser::read()
	 *      -------------------------                   --------
	 *                  |                      (Only possible if an application is loaded)
	 *                  |                                   |
	 *                  |    Find panels by matching their IDs to INIs key <--- set file values
	 *                  |                                   |
	 *                  |                                   v      (ready for user interaction)
	 *                  |                   ________________________________________
	 *                  |                   |INI file contents are displayed in GUI|
	 *                  v                   ----------------------------------------
	 *   _________________________________
	 *   |  Build GUI elements from XML  |  <----- Scan file system for application XMLs
	 *   ---------------------------------           (read and check: XMLParser class)
	 *                  |                                  ^
	 *         Find panels in XML  <----- inishell.cc      |_ WorkflowPanel::scanFoldersForApps()
	 *                  |
	 *              Build panels  <----- panels.cc ----->  The panels' constructors
	 *                  |
	 *        Set panels' properties  <----- the panels' setOptions() member functions
	 *                  |
	 * Give panels IDs to interact with INI changes  <----- Atomic::setEmphasisWidget()
	 *                  |                                     ^
	 *                  V                                     |
	 *            _______________             (Base class for all panels, uniform access to
	 *            |GUI is built!|             styling, section/key; used to find all panels)
	 *            ---------------
	 *                  |
	 *                  v
	 *          Set INI values from:
	 *          --------------------
	 *
	 *         XML              INI  <----- INIParser class
	 *   (default values)  (file system)                      VALIDATION
	 *          |                |                           (set styles)
	 *          |                |                                ^
	 *          v                v                               /
	 *    _____________________________________                 /
	 *    |      Panel's onPropertySet()      |  -----> CHECK VALUE
	 *    -------------------------------------           |
	 *          ^                ^        (Property "no_ini" not set? Value not empty?)
	 *          |                |                        |
	 *          |                |                        v    MainPanel::setIniValuesFromGui()
	 *         GUI              CODE                    OUTPUT            |
	 * (user interaction)  (reactive changes)        (on request)         |
	 *          ^                                         |               v
	 *          |           Run through panels by IDs (= INI keys) and fill INIParser
	 *   Detect changes                                   |
	 *   (when closing)  <----- INIParser::operator==     v
	 *                                               Output to file  <----- INIParser class
	 */

	//The following is needed especially for the tinyexpr external library when a locale is set
	//interpreting something else than '.' as decimal separator. Here we override this so that
	//expressions are parsed assuming a dot (i. e. not a comma).
	//(must be set from inside event loop?)
	setlocale(LC_NUMERIC, "C");

	status_timer_ = new QTimer(this); //for temporary status messages
	status_timer_->setSingleShot(true);
	connect(status_timer_, &QTimer::timeout, this, &MainWindow::clearStatus);

	logger_.logSystemInfo();
	for (auto &err : errors) //errors from main.cc before the Logger existed
		logger_.log(err, "error");

	/* create basic GUI items */
	this->setUnifiedTitleAndToolBarOnMac(true);
	this->setWindowTitle(QCoreApplication::applicationName());
	createMenu();
	createToolbar();
	createStatusbar();

	/* create and size child windows */
	preview_ = new PreviewWindow(this);
	settings_window_ = new SettingsWindow(this);
	help_window_ = new HelpWindow(this);
	sizeAllWindows();

	/* create the dynamic GUI area */
	control_panel_ = new MainPanel(this);
	this->setCentralWidget(control_panel_);
	ini_.setLogger(&logger_);
	if (errors.isEmpty())
		setStatus(tr("Ready."), "info");
	else
		setStatus(tr("Errors occurred on startup"), "error");
}

/**
 * @brief Destructor for the main window.
 * @details This function is called after all safety checks have already been performed.
 */
MainWindow::~MainWindow()
{ //safety checks are performed in closeEvent()
	setWindowSizeSettings(); //put the main window sizes into the settings XML
	saveSettingsFile();
	delete mouse_events_toolbar_;
	delete mouse_events_statusbar_;
}

/**
 * @brief Retrieve all panels that handle a certain INI key.
 * @param[in] ini_key The INI key to find.
 * @return A list of all panels that handle the INI key.
 */
QList<Atomic *> MainWindow::getPanelsForKey(const QString &ini_key) const
{
	QList<Atomic *> panel_list( control_panel_->getSectionTab()->findChildren<Atomic *>(Atomic::getQtKey(ini_key)) );
	for (auto it = panel_list.begin(); it != panel_list.end(); ++it) {
		//groups don't count towards finding INI keys (for this reason, they additionally
		//have the "no_ini" key set):
		if (qobject_cast<Group *>(*it))
			panel_list.erase(it);
	}
	return panel_list;
}

/**
 * @brief Save the current GUI to an INI file.
 * @details This function calls the underlying function that does this and displays a message if
 * the user has neglected to set some mandatory INI values.
 * @param[in] filename The file to save to. If not given, the current INI file will be used.
 */
bool MainWindow::saveIni(const QString &filename)
{
	/*
	 * We keep the original INIParser as-is, i. e. we always keep the INI file as it was loaded
	 * originally. This is solemnly to be able to check if anything has changed through user
	 * interaction, and if yes, warn before closing.
	 */
	INIParser gui_ini = ini_; //an INIParser is safe to copy around - this is a deep copy
	gui_ini.clear(true);  //only keep unknown keys (which are transported from input to output)
	const QString missing( control_panel_->getSectionTab()->setIniValuesFromGui(&gui_ini) );

	if (!missing.isEmpty()) {
		QMessageBox msgMissing;
		msgMissing.setWindowTitle("Warning ~ " + QCoreApplication::applicationName());
		msgMissing.setText(tr("<b>Missing mandatory INI values.</b>"));
		msgMissing.setInformativeText(tr(
		    "Some non-optional INI keys are not set.\nSee details for a list or go back to the GUI and set all highlighted fields."));
		msgMissing.setDetailedText(tr("Missing INI keys:\n") + missing);
		msgMissing.setIcon(QMessageBox::Warning);
		msgMissing.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
		msgMissing.setDefaultButton(QMessageBox::Cancel);
		const int clicked = msgMissing.exec();
		if (clicked == QMessageBox::Cancel) {
			const QStringList first_missing( missing.split(",\n").at(0).split("::") );
			control_panel_->getSectionTab()->showPanel(first_missing.at(0), first_missing.at(1));
			return false;
		}
	}
	//if no file is specified we save to the currently open INI file (save vs. save as):
	const QString new_filename( filename.isEmpty()? gui_ini.getFilename() : filename );
	gui_ini.writeIni(new_filename);
	setStatus(tr("Saved to ") + new_filename, "info");
	return true;
}

/**
 * @brief Save the currently set values to a new INI file.
 */
void MainWindow::saveIniAs()
{
	QString start_path( getSetting("auto::history::last_ini", "path") );
	if (start_path.isEmpty())
		start_path = getSetting("auto::history::last_preview_write", "path");
	if (start_path.isEmpty())
		start_path = QDir::currentPath();

	//QFileDialog::DontUseNativeDialog is required to get rid of a bug, but then the mounts on macOS do not work (cf. issue 872):
	//QTBUG: https://bugreports.qt.io/browse/QTBUG-67866?focusedCommentId=408617&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-408617
	const QString filename( QFileDialog::getSaveFileName(this, tr("Save INI file"),
	    start_path + "/" + ini_filename_->text(),
		"INI files (*.ini *.INI);;All files (*)", nullptr) );
	if (filename.isNull()) //user cancelled out from the file picker
		return;
	bool perform_close = saveIni(filename);
	if (!perform_close) //user cancelled out from the "unsaved changes" warning
		return;
	ini_.setFilename(filename); //the new file is the new current file like in all programs
	ini_filename_->setText(filename);
	autoload_->setVisible(true); //if started from an empty GUI, this could still be disabled
	toolbar_save_ini_->setEnabled(true); //toolbar entry
	file_save_ini_->setEnabled(true); //menu entry

	setSetting("auto::history::last_ini", "path", QFileInfo( filename ).absoluteDir().path());
}

/**
 * @brief Select a path for an INI file to be opened, and then open it.
 */
void MainWindow::openIni()
{
	QString start_path( getSetting("auto::history::last_ini", "path") );
	if (start_path.isEmpty())
		start_path = QDir::currentPath();

	const QString path( QFileDialog::getOpenFileName(this, tr("Open INI file"), start_path,
		"INI files (*.ini);;All files (*)", nullptr, QFileDialog::DontConfirmOverwrite) );
	if (path.isNull()) //cancelled
		return;
	openIni(path);

	setSetting("auto::history::last_ini", "path", QFileInfo( path ).absoluteDir().path());
}

/**
 * @brief Set the loaded panels' values from an INI file.
 * @param[in,out] ini The INI file in form of an INIParser (usually the main one).
 * This object may be modified when keys are recognized to be unknown to the application.
 * @return True if all INI keys are known to the loaded XML.
 */
bool MainWindow::setGuiFromIni(INIParser &ini)
{
#ifdef DEBUG
	QElapsedTimer gui_set_timer;
	gui_set_timer.start();
#endif //def DEBUG

	bool all_ok = true;
	bool first_error_message = true;
	for (auto &sec : *ini.getSections()) { //run through sections in INI file

		ScrollPanel *tab_scroll = getControlPanel()->getSectionTab()->getSectionScrollArea(sec.getName(),
		    QString(), QString(), true); //get the corresponding tab of our GUI
		if (tab_scroll == nullptr) //check if a dynamic section exists that can create it
			tab_scroll = getControlPanel()->getSectionTab()->tryDynamicSection(sec.getName());

		if (tab_scroll != nullptr) { //section exists in GUI
			const auto kv_list( sec.getKeyValueList() );
			for (size_t ii = 0; ii < kv_list.size(); ++ii) {
				//find the corresponding panel, and try to create it for dynamic panels
				//(e. g. Selector, Replicator):
				QWidgetList widgets( findPanel(tab_scroll, sec, *sec[ii]) );
				if (!widgets.isEmpty()) {
					for (int jj = 0; jj < widgets.size(); ++jj) { //multiple panels can share the same key
						widgets.at(jj)->setProperty("ini_value", sec[ii]->getValue());
						/*
						 * The INIParser assigns comments around keys to said key in order to reproduce the
						 * file. We preserve this info right here in the panel itself, because when we build
						 * the part of the INI that comes from the GUI we would have to go looking for the
						 * corresponding comments otherwise.
						 */
						widgets.at(jj)->setProperty("comment_inline", sec[ii]->getInlineComment());
						widgets.at(jj)->setProperty("comment_block", sec[ii]->getBlockComment());
					}
				} else {
					sec[ii]->setIsUnknownToApp();
					writeGuiFromIniHeader(first_error_message, ini);
					logger_.log(tr("%1 does not know INI key \"").arg(current_application_) +
					    sec.getName() + Cst::sep + sec[ii]->getKey() + "\"", "warning");
					all_ok = false;
				}
			} //endfor kv
		} else { //section does not exist
			auto kv_list( sec.getKeyValueList() );
			for (size_t ii = 0; ii < kv_list.size(); ++ii) //mark all keys in unknown section as unknown
				sec[ii]->setIsUnknownToApp();
			writeGuiFromIniHeader(first_error_message, ini);
			log(tr(R"(%1 does not know INI section "[%2]")").arg(
			    current_application_, sec.getName()), "warning");
			all_ok = false;
		} //endif section exists

	} //endfor sec

#ifdef DEBUG
	std::cout << "Setting GUI took " << gui_set_timer.elapsed() << " ms" << std::endl;
#endif //def DEBUG
	return all_ok;
}

/**
 * @brief Apply changed appearance settings to the main window..
 */
void MainWindow::resetAppearance(const bool &restart)
{
	sizeAllWindows(); //we must set all this also when restarting, because otherwise
	delete toolbar_; //it would be saved right back as-is on program exit
	createToolbar();
	control_panel_->setSplitterSizes();

	if (restart) { //hard reset - will apply appearance settings
		qApp->processEvents(); //e. g. fire the resize timers on macOS
		qApp->quit();
		QProcess::startDetached(qApp->arguments().at(0), qApp->arguments());
	}
}

/**
 * @brief Open an INI file and set the GUI to it's values.
 * @param[in] path The INI file to open.
 * @param[in] is_autoopen Is the INI being loaded through the autoopen mechanism?
 */
void MainWindow::openIni(const QString &path, const bool &is_autoopen, const bool &fresh)
{
	this->getControlPanel()->getWorkflowPanel()->setEnabled(false); //hint at INIshell processing...
	setStatus(tr("Reading INI file..."), "info", true);
	refreshStatus(); //necessary if heavy operations follow
	if (fresh)
		clearGui();

#ifdef DEBUG
	QElapsedTimer ini_parse_timer;
	ini_parse_timer.start();
#endif //def DEBUG
	const bool success = ini_.parseFile(path); //load the file into the main INI parser
#ifdef DEBUG
	std::cout << "Parsing INI took " << ini_parse_timer.elapsed() << " ms" << std::endl;
#endif //def DEBUG
	if (!setGuiFromIni(ini_)) //set the GUI to the INI file's values
		setStatus(tr("INI file read with unknown keys"), "warning");
	else
		setStatus(tr("INI file read ") + (success? tr("successfully") : tr("with warnings")),
			(success? "info" : "warning"), false, Cst::msg_length); //ill-formatted lines in INI file
	toolbar_save_ini_->setEnabled(true);
	file_save_ini_->setEnabled(true);
	autoload_->setVisible(true);
	ini_filename_->setText(path);
	autoload_box_->setText(tr("autoload this INI for ") + current_application_);
	if (!is_autoopen) //when a user clicks an INI file to open it we ask anew whether to autoopen
		autoload_box_->setCheckState(Qt::Unchecked);

	this->getControlPanel()->getWorkflowPanel()->setEnabled(true);
	QApplication::alert( this ); //notify the user that the task is finished
}

/**
 * @brief Close the currently opened INI file.
 * @details This function checks whether the user has made changes to the INI file and if yes,
 * allows to save first, discard changes, or cancel the operation.
 * @return True if the INI file was closed, false if the user has cancelled.
 */
bool MainWindow::closeIni()
{
	INIParser gui_ini( ini_ );
	(void) control_panel_->getSectionTab()->setIniValuesFromGui(&gui_ini);
	if (ini_ != gui_ini) {
		static const QString msg_marker( "An application has been opened," );
		QString do_not_show_setting( "user::warnings::warn_unsaved_ini" );
		if (ini_.getEqualityCheckMsg().startsWith(msg_marker))
			do_not_show_setting = "user::warnings::warn_unsaved_empty";

		if (getSetting(do_not_show_setting, "value") == "TRUE") {
			/*
			 * We leave the original INIParser - the one that holds the values like they were
			 * originally read from an INI file - intact and make a copy of it. The currently
			 * set values from the GUI are loaded into the copy, and then the two are compared
			 * for changes. This way we don't display a "settings may be lost" warning if in
			 * fact nothing has changed, resp. the changes cancelled out.
			 */

			QString msg( tr("INI settings will be lost") );
			if (ini_.getEqualityCheckMsg().startsWith(msg_marker))
				msg = tr("INI settings not saved yet");
			const QString informative( "Some INI keys will be lost if you don't save the current INI file." );
			const QFlags<QMessageBox::StandardButton> buttons( QMessageBox::Save | QMessageBox::Cancel | QMessageBox::Discard );
			RememberDialog msgNotSaved(do_not_show_setting, msg, informative,
				ini_.getEqualityCheckMsg(), buttons, this);

			const int clicked = msgNotSaved.exec();
			if (clicked == QMessageBox::Cancel)
				return false;
			if (clicked == QMessageBox::Save)
				(void) saveIni();
		} //endif help_loaded
	} //endif ini_ != gui_ini

	ini_.clear();
	toolbar_save_ini_->setEnabled(false);
	file_save_ini_->setEnabled(false);
	ini_filename_->setText(QString());
	autoload_->setVisible(false);
	return true;
}

/**
 * @brief Reset the GUI to default values.
 * @details This function checks whether there were any changes to the INI values
 * (if yes, the user can save first or discard or cancel), then clears the whole GUI.
 */
void MainWindow::clearGui(const bool &set_default)
{
	const bool perform_close = closeIni();
	if (!perform_close) //user clicked 'cancel'
		return;
	getControlPanel()->clearGui(set_default);
	control_panel_->getSectionTab()->clearDynamicTabs();
	ini_filename_->setText(QString());
	autoload_->setVisible(false);
}

/**
 * @brief Store the main window sizes in the settings XML.
 */
void MainWindow::setWindowSizeSettings()
{
	setSplitterSizeSettings();

	setSetting("auto::sizes::window_" + QString::number(dim::window_type::PREVIEW),
	    "width", QString::number(preview_->width()));
	setSetting("auto::sizes::window_" + QString::number(dim::window_type::PREVIEW),
	    "height", QString::number(preview_->height()));
	setSetting("auto::sizes::window_" + QString::number(dim::window_type::LOGGER),
	    "width", QString::number(logger_.width()));
	setSetting("auto::sizes::window_" + QString::number(dim::window_type::LOGGER),
	    "height", QString::number(logger_.height()));
	setSetting("auto::sizes::window_" + QString::number(dim::window_type::MAIN_WINDOW),
	    "width", QString::number(this->width()));
	setSetting("auto::sizes::window_" + QString::number(dim::window_type::MAIN_WINDOW),
	    "height", QString::number(this->height()));
	setSetting("auto::sizes::window_" + QString::number(dim::window_type::SETTINGS),
		"width", QString::number(settings_window_->width()));
	setSetting("auto::sizes::window_" + QString::number(dim::window_type::SETTINGS),
		"height", QString::number(settings_window_->height()));
	setSetting("auto::sizes::window_" + QString::number(dim::window_type::HELP),
		"width", QString::number(help_window_->width()));
	setSetting("auto::sizes::window_" + QString::number(dim::window_type::HELP),
		"height", QString::number(help_window_->height()));

	//remember the toolbar position:
	setSetting("auto::position::toolbar", "position", QString::number(this->toolBarArea(toolbar_)));
}

/**
 * @brief Remember the main splitter's space distribution.
 */
void MainWindow::setSplitterSizeSettings()
{
	const QList<int> splitter_sizes(control_panel_->getSplitterSizes());
	setSetting("auto::sizes::splitter_workflow", "size", QString::number(splitter_sizes.at(0)));
	setSetting("auto::sizes::splitter_mainpanel", "size", QString::number(splitter_sizes.at(1)));
}

/**
 * @brief Create a context menu for the toolbar.
 */
void MainWindow::createToolbarContextMenu()
{
	/* allow user to enable toolbar dragging */
	auto *fix_toolbar_position = new QAction(tr("Fix toolbar position"), toolbar_);
	fix_toolbar_position->setCheckable(true);
	fix_toolbar_position->setChecked(getSetting("user::appearance::fix_toolbar_pos", "value") == "TRUE");
	toolbar_context_menu_.addAction(fix_toolbar_position);
}

/**
 * @brief Event handler for the "View::Preview" menu: open the preview window.
 * @details This is public so that the Preview can be shown from other windows
 * (e. g. the Logger).
 */
void MainWindow::viewPreview()
{
	if (view_preview_->isEnabled()) {
		preview_->addIniTab();
		preview_->show();
		preview_->raise();
	}
}

/**
 * @brief Load the user help XML from the resources onto the GUI.
 * @param[in] tab_name Optional tab name to focus.
 * @param[in] frame_name Optional frame name to highlight.
 * @param[in] dev Load developer's help?
 */
void MainWindow::loadHelp(const QString &tab_name, const QString &frame_name, const bool &dev)
{
	help_window_->loadHelp(dev);
	if (!tab_name.isEmpty())
		(void) help_window_->getSectionTab()->showPanel(tab_name, frame_name, true);
}

/**
 * @brief Open an XML file containing an application/simulation.
 * @param[in] path Path to the file to open.
 * @param[in] app_name When looking for applications, the application name was parsed and put
 * into a list. This is the app_name so we don't have to parse again.
 * @param[in] fresh If true, reset the GUI before opening a new application.
 * @param[in] is_settings_dialog Is it the settings that are being loaded?
 */
void MainWindow::openXml(const QString &path, const QString &app_name, const bool &fresh)
{
	if (fresh) {
		const bool perform_close = closeIni();
		if (!perform_close)
			return;
		control_panel_->clearGuiElements();
	}

	current_application_ = app_name;
	current_path_ = path;

	if (QFile::exists(path)) {
		setStatus(tr("Reading application XML..."), "info", true);
		refreshStatus();
		XMLReader xml;
		QString xml_error;
		const QString autoload_ini( xml.read(path, xml_error) );
		if (!xml_error.isNull()) {
			xml_error.chop(1); //trailing \n
			Error(tr("Errors occurred when parsing the XML configuration file"),
			    tr("File: \"") + QDir::toNativeSeparators(path) + "\"", xml_error);
		}
		if (xml.useExternal()) {
			setStatus(tr("External application found, see log for more information..."), "info", true);
			refreshStatus();
			sleep(1);
		}		
		setStatus("Building GUI...", "info", true);
		refreshStatus();
		buildGui(xml.getXml());
		setStatus("Ready.", "info", false);
		refreshStatus();
		control_panel_->getWorkflowPanel()->buildWorkflowPanel(xml.getXml());
		if (!autoload_ini.isEmpty()) {
			if (QFile::exists(autoload_ini))
				openIni(autoload_ini);
			else
				log(QString("Can not load INI file \"%1\" automatically because it does not exist.").arg(
				    QDir::toNativeSeparators(autoload_ini)), "error");
		}
	} else {
		topLog(tr("An application or simulation file that has previously been found is now missing. Right-click the list to refresh."),
		     "error");
		setStatus(tr("File has been removed"), "error");
		return;
	}

	//run through all INIs that were saved to be autoloaded and check if it's the application we are opening:
	for (auto ini_node = global_xml_settings.firstChildElement().firstChildElement("user").firstChildElement(
	    "autoload").firstChildElement("ini");
	    !ini_node.isNull(); ini_node = ini_node.nextSiblingElement("ini")) {
		if (ini_node.attribute("application").toLower() == app_name.toLower()) {
			autoload_box_->blockSignals(true); //don't re-save the setting we just fetched
			autoload_box_->setCheckState(Qt::Checked);
			autoload_box_->setText(tr("autoload this INI for ") + current_application_);
			autoload_box_->blockSignals(false);
			openIni(ini_node.text(), true); //TODO: delete if non-existent
			break;
		}
	}

	toolbar_clear_gui_->setEnabled(true);
	gui_reset_->setEnabled(true);
	gui_clear_->setEnabled(true);
	toolbar_save_ini_as_->setEnabled(true);
	file_save_ini_as_->setEnabled(true);
	toolbar_open_ini_->setEnabled(true); //toolbar entry
	file_open_ini_->setEnabled(true); //menu entry
	view_preview_->setEnabled(true);
	toolbar_preview_->setEnabled(true);

	auto *file_view = getControlPanel()->getWorkflowPanel()->getFilesystemView();
	file_view->setEnabled(true);
	auto *path_label = file_view->getInfoLabel();
	path_label->setText(file_view->getInfoLabel()->property("path").toString());
	path_label->setWordWrap(true);
	path_label->setStyleSheet("QLabel {font-style: italic}");
	QApplication::alert(this); //notify the user that the task is finished
}

/**
 * @brief Find the panels that control a certain key/value pair.
 * @details This function includes panels that could be created by dynamic panels (Selector etc.).
 * @param[in] parent The parent panel or window to search, since usually we have some information
 * about where to find it. This function is used when iterating through INIParser elements
 * to load the GUI with values.
 * @param[in] section The section to find.
 * @param[in] keyval The key/value pair to find.
 * @return A list of all panels controlling the key/value pair.
 */
QWidgetList MainWindow::findPanel(QWidget *parent, const Section &section, const KeyValue &keyval) const
{
	//first, check if there is a panel for it loaded and available:
	QWidgetList panels = findSimplePanel(parent, section, keyval);
	//if not found, check if one of our Replicators can create it:
	bool found = false;
	if (panels.isEmpty())
		found = prepareReplicator(parent, section, keyval);
	if (found)
		panels = findSimplePanel(parent, section, keyval);

	//if still not found, check if one of our Selectors can create it:
	const qsizetype panel_count = parent->findChildren<Atomic *>().count();
	if (panels.isEmpty())
		panels = prepareSelector(parent, section, keyval);
	//TODO: For the current MeteoIO XML the order is important here so that a TIME filter
	//does not show up twice (because both the normal and time filter's panel fit the pattern).
	//Cleaning up here would also increase performance.
	if (parent->findChildren<Atomic *>().count() != panel_count) //new elements were created
		return findPanel(parent, section, keyval); //recursion through Replicators that create Selectors that...
	return panels; //no more suitable dynamic panels found
}

/**
 * @brief Find the panels that control a certain key/value pair.
 * @details This function does not include dynamic panels (Selector etc.) and searches only
 * what has already been created.
 * @param[in] parent The parent panel or window to search, because usually we have some information.
 * @param[in] section Section to find.
 * @param[in] keyval Key-value pair to find.
 * @return A list of all panels controlling the key/value pair.
 */
QWidgetList MainWindow::findSimplePanel(QWidget *parent, const Section &section, const KeyValue &keyval)
{
	const QString id(section.getName() + Cst::sep + keyval.getKey());
	/* simple, not nested, keys */
	return parent->findChildren<QWidget *>(Atomic::getQtKey(id));
}

/**
 * @brief Build the main window's menu items.
 */
void MainWindow::createMenu()
{
	/* FILE menu */
	QMenu *menu_file = this->menuBar()->addMenu(tr("&File"));
	file_open_ini_ = new QAction(getIcon("document-open"), tr("&Open INI file..."), menu_file);
	menu_file->addAction(file_open_ini_); //note that this does not take ownership
	connect(file_open_ini_, &QAction::triggered, this, [=]{ toolbarClick("open_ini"); });
	file_open_ini_->setShortcut(QKeySequence::Open);
	file_save_ini_ = new QAction(getIcon("document-save"), tr("&Save INI file"), menu_file);
	file_save_ini_->setShortcut(QKeySequence::Save);
	menu_file->addAction(file_save_ini_);
	file_save_ini_->setEnabled(false); //enable after an INI is opened
	connect(file_save_ini_, &QAction::triggered, this, [=]{ toolbarClick("save_ini"); });
	file_save_ini_as_ = new QAction(getIcon("document-save-as"), tr("Save INI file &as..."), menu_file);
	file_save_ini_as_->setShortcut(QKeySequence::SaveAs);
	menu_file->addAction(file_save_ini_as_);
	file_save_ini_as_->setEnabled(false);
	connect(file_save_ini_as_, &QAction::triggered, this, [=]{ toolbarClick("save_ini_as"); });
	menu_file->addSeparator();
	auto *file_quit_ = new QAction(getIcon("application-exit"), tr("&Exit"), menu_file);
	file_quit_->setShortcut(QKeySequence::Quit);
	file_quit_->setMenuRole(QAction::QuitRole);
	menu_file->addAction(file_quit_);
	connect(file_quit_, &QAction::triggered, this, &MainWindow::quitProgram);

	/* GUI menu */
	QMenu *menu_gui = this->menuBar()->addMenu(tr("&GUI"));
	gui_reset_ = new QAction(getIcon("document-revert"), tr("&Reset to default values"), menu_gui);
	menu_gui->addAction(gui_reset_);
	gui_reset_->setEnabled(false);
	gui_reset_->setShortcut(Qt::CTRL | Qt::Key_Backspace);
	connect(gui_reset_, &QAction::triggered, this, [=]{ toolbarClick("reset_gui"); });
	gui_clear_ = new QAction(getIcon("edit-delete"), tr("&Clear"), menu_gui);
	menu_gui->addAction(gui_clear_);
	gui_clear_->setEnabled(false);
	gui_clear_->setShortcut(Qt::CTRL | Qt::SHIFT | Qt::Key_Backspace);
	connect(gui_clear_, &QAction::triggered, this, [=]{ toolbarClick("clear_gui"); });
	menu_gui->addSeparator();
	gui_close_all_ = new QAction(getIcon("window-close"), tr("Close all content"), menu_gui);
	menu_gui->addAction(gui_close_all_);
	connect(gui_close_all_, &QAction::triggered, this, &MainWindow::resetGui);

	/* VIEW menu */
	QMenu *menu_view = this->menuBar()->addMenu(tr("&View"));
	view_preview_ = new QAction(getIcon("document-preview"), tr("P&review"), menu_view);
	menu_view->addAction(view_preview_);
	view_preview_->setShortcut(QKeySequence::Print);
	view_preview_->setEnabled(false); //enable with loaded application
	connect(view_preview_, &QAction::triggered, this, &MainWindow::viewPreview);
	auto *view_log = new QAction(getIcon("utilities-system-monitor"), tr("&Log"), menu_view);
	menu_view->addAction(view_log);
	connect(view_log, &QAction::triggered, this, &MainWindow::viewLogger);
	view_log->setShortcut(Qt::CTRL | Qt::Key_L);
	auto *window_toggle_workflow = new QAction(getIcon("application-menu"), tr("Toggle wor&kflow"), menu_view);
	menu_view->addAction(window_toggle_workflow);
	connect(window_toggle_workflow, &QAction::triggered, this, &MainWindow::toggleWorkflow);
	window_toggle_workflow->setShortcut(Qt::CTRL | Qt::Key_K);
	auto *view_refresh = new QAction(getIcon("view-refresh"), tr("&Refresh applications"), menu_view);
	menu_view->addAction(view_refresh);
	connect(view_refresh, &QAction::triggered, this,
	    [=]{ control_panel_->getWorkflowPanel()->scanFoldersForApps(); });
	view_refresh->setShortcut(QKeySequence::Refresh);
	menu_view->addSeparator();
	auto *view_settings = new QAction(getIcon("preferences-system"), tr("&Settings"), menu_view);
	view_settings->setMenuRole(QAction::PreferencesRole);
	menu_view->addAction(view_settings);
	connect(view_settings, &QAction::triggered, this, &MainWindow::viewSettings);
#if defined Q_OS_MAC //only Mac has this as of now: https://doc.qt.io/archives/qt-4.8/qkeysequence.html
//equivalent at runtime: if (QKeySequence(QKeySequence::Preferences).toString(QKeySequence::NativeText).isEmpty())
	view_settings->setShortcut(QKeySequence::Preferences);
#else
	view_settings->setShortcut(Qt::Key_F3);
#endif

#ifdef DEBUG
	createDebugMenu();
#endif //def DEBUG

	/* HELP menu */
#if !defined Q_OS_MAC //use extra tab bar that is pushed to the right
	auto *menu_help_bar = new QMenuBar(this->menuBar());
	this->menuBar()->setCornerWidget(menu_help_bar); //push help menu to the right
	QMenu *menu_help_main = menu_help_bar->addMenu(tr("&Help"));
	auto *menu_help = new QAction(getIcon("help-contents"), tr("&Help"), menu_help_main);
	menu_help_main->addAction(menu_help);
	auto *help_about = new QAction(getIcon("help-about"), tr("&About"), menu_help_main);
	menu_help_main->addAction(help_about);
#else //normal menu that can be re-ordered by Qt/macOS
	auto *menu_help_main = this->menuBar()->addMenu("&Help");
	auto *menu_help = new QAction(getIcon("help-contents"), tr("&Help"), menu_help_main);
	menu_help_main->addAction(menu_help);
	auto *help_about = new QAction(getIcon("help-about"), tr("&About"), menu_help_main);
	menu_help_main->addAction(help_about);
#endif
	connect(menu_help, &QAction::triggered, this, [=]{ loadHelp(); });
	connect(help_about, &QAction::triggered, this, &MainWindow::helpAbout);
	menu_help->setShortcut(QKeySequence::HelpContents);
	menu_help->setMenuRole(QAction::ApplicationSpecificRole); //we can not use the Spotlight search from Qt
	help_about->setMenuRole(QAction::AboutRole);
	help_about->setMenuRole(QAction::ApplicationSpecificRole);
	help_about->setShortcut(QKeySequence::WhatsThis);
}

/**
 * @brief Create the main window's toolbar.
 */
void MainWindow::createToolbar()
{
	/* toolbar */
	toolbar_ = new QToolBar("Shortcuts toolbar");
	toolbar_->setContextMenuPolicy(Qt::CustomContextMenu);
	createToolbarContextMenu();
	connect(toolbar_, &QToolBar::customContextMenuRequested, this, &MainWindow::onToolbarContextMenuRequest);
	toolbar_->setMovable(getSetting("user::appearance::fix_toolbar_pos", "value") == "FALSE");
	toolbar_->setFloatable(false);

	bool toolbar_success;
	Qt::ToolBarArea toolbar_area = static_cast<Qt::ToolBarArea>( //restore last toolbar position
	    getSetting("auto::position::toolbar", "position").toInt(&toolbar_success));
	if (!toolbar_success)
		toolbar_area = Qt::ToolBarArea::TopToolBarArea;
	this->addToolBar(toolbar_area, toolbar_);

	/* user interaction buttons */
	toolbar_->setIconSize(QSize(32, 32));
	toolbar_open_ini_ = toolbar_->addAction(getIcon("document-open"), tr("Open INI file"));
	connect(toolbar_open_ini_, &QAction::triggered, this, [=]{ toolbarClick("open_ini"); });
	toolbar_open_ini_->setEnabled(false); //toolbar entry
	file_open_ini_->setEnabled(false); //menu entry
	toolbar_->addSeparator();
	toolbar_save_ini_ = toolbar_->addAction(getIcon("document-save"), tr("Save INI"));
	toolbar_save_ini_->setEnabled(false); //enable when an INI is open
	connect(toolbar_save_ini_, &QAction::triggered, this, [=]{ toolbarClick("save_ini"); });
	toolbar_save_ini_as_ = toolbar_->addAction(getIcon("document-save-as"), tr("Save INI file as"));
	toolbar_save_ini_as_->setEnabled(false);
	connect(toolbar_save_ini_as_, &QAction::triggered, this, [=]{ toolbarClick("save_ini_as"); });
	toolbar_preview_ = toolbar_->addAction(getIcon("document-preview"), tr("Preview INI"));
	toolbar_preview_->setEnabled(false);
	connect(toolbar_preview_, &QAction::triggered, this, [=]{ toolbarClick("preview"); });
	toolbar_->addSeparator();
	toolbar_clear_gui_ = toolbar_->addAction(getIcon("document-revert"), tr("Clear INI settings"));
	connect(toolbar_clear_gui_, &QAction::triggered, this, [=]{ toolbarClick("reset_gui"); });
	toolbar_clear_gui_->setEnabled(false);

	/* info label and autoload checkbox */
	QWidget *spacer = new QWidget;
	spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	QWidget *small_spacer = new QWidget;
	small_spacer->setFixedWidth(25);
	ini_filename_ = new QLabel();
	ini_filename_->setProperty("mouseclick", "open_ini");
	mouse_events_toolbar_ = new MouseEventFilter;
	ini_filename_->installEventFilter(mouse_events_toolbar_);
	ini_filename_->setCursor(QCursor(Qt::PointingHandCursor));
	autoload_box_= new QCheckBox;

	toolbar_->addWidget(spacer);
	toolbar_->addWidget(ini_filename_);
	toolbar_->addWidget(small_spacer);
	connect(autoload_box_, &QCheckBox::stateChanged, this, &MainWindow::onAutoloadCheck);

	autoload_ = toolbar_->addWidget(autoload_box_);
	autoload_->setVisible(false);
	toolbar_->addAction(autoload_);
}

/**
 * @brief Create the main window's status bar.
 */
void MainWindow::createStatusbar()
{ //we use our own because Qt's statusBar is for temporary messages only (they will vanish)
	auto *spacer_widget = new QWidget;
	spacer_widget->setFixedSize(5, 0);
	status_label_ = new QLabel;
	status_label_->setProperty("mouseclick", "open_log");
	mouse_events_statusbar_ = new MouseEventFilter;
	status_label_->installEventFilter(mouse_events_statusbar_);
	status_label_->setCursor(QCursor(Qt::PointingHandCursor));
	status_icon_ = new QLabel;
	statusBar()->addWidget(spacer_widget);
	statusBar()->addWidget(status_label_);
	statusBar()->addPermanentWidget(status_icon_);
}

/**
 * @brief Resize the separate child windows.
 */
void MainWindow::sizeAllWindows()
{
	dim::setDimensions(this, dim::MAIN_WINDOW);
#if defined Q_OS_MAC
	QTimer::singleShot(1, this, [=]{ dim::setDimensions(help_window_, dim::HELP); });
	QTimer::singleShot(1, this, [=]{ dim::setDimensions(preview_, dim::PREVIEW); });
#else
	dim::setDimensions(help_window_, dim::HELP);
	dim::setDimensions(preview_, dim::PREVIEW);
#endif
	dim::setDimensions(settings_window_, dim::SETTINGS);
	dim::setDimensions(&logger_, dim::LOGGER);
}

/**
 * @brief Display a text in the main window's status bar.
 * @details Events such as hovering over a menu may clear the status. It is meant
 * for temporary messages.
 * @param[in] message The text to display.
 * @param[in] time Time in ms to display the text for.
 */
void MainWindow::setStatus(const QString &message, const QString &color, const bool &status_light, const int &time_msec)
{ //only temporary messages are possible via statusBar()->showMessage
	status_label_->setText(message);
	status_label_->setStyleSheet("QLabel {color: " + colors::getQColor(color).name() + "}");
	setStatusLight(status_light);
	status_timer_->stop(); //stop pending clearing
	if (time_msec > 0 && time_msec < 5000) {
		status_timer_->setInterval(time_msec); //start new interval
		status_timer_->start();
	}
}

/**
 * @brief Set the status light indicating activity on or off.
 * @param[in] on True if it should be active, false for inactive.
 */
void MainWindow::setStatusLight(const bool &on)
{
	const QIcon icon((on? ":/icons/active.svg" : ":/icons/inactive.svg"));
	status_icon_->setPixmap(icon.pixmap(QSize(16, 16)));
} //don't use QPixmap directly to work on macOS

/**
 * @brief Refresh the status label.
 * @details Heavy operations can block the painting which these calls initiate manually.
 */
void MainWindow::refreshStatus()
{
	status_label_->adjustSize();
	status_label_->repaint();
}

/**
 * @brief Clear the main status label.
 */
void MainWindow::clearStatus()
{
	status_label_->setText(QString());
}

/**
 * @brief Event handler for the "File::Quit" menu: quit the main program.
 */
void MainWindow::quitProgram()
{
	QApplication::quit();
}

/**
 * @brief Event handler for the "GUI::resetGui" menu: reset thr GUI completely.
 * @details This is the only action that resets the GUI to the starting point.
 */
void MainWindow::resetGui()
{
	toolbarClick("clear_gui");
	control_panel_->clearGuiElements();
	control_panel_->displayInfo();
	current_application_ = QString();
	this->setWindowTitle(QCoreApplication::applicationName());
	auto *file_view = getControlPanel()->getWorkflowPanel()->getFilesystemView();
	file_view->setEnabled(false);
	auto *path_label = file_view->getInfoLabel();
	path_label->setText(tr("Open an application or simulation before opening INI files."));
	path_label->setWordWrap(true);
	path_label->setStyleSheet("QLabel {color: " + colors::getQColor("important").name() + "}");

	toolbar_open_ini_->setEnabled(false);
	toolbar_save_ini_->setEnabled(false);
	toolbar_save_ini_as_->setEnabled(false);
	toolbar_clear_gui_->setEnabled(false);
	file_open_ini_->setEnabled(false);
	file_save_ini_->setEnabled(false);
	file_save_ini_as_->setEnabled(false);
	gui_reset_->setEnabled(false);
	gui_clear_->setEnabled(false);
	view_preview_->setEnabled(false);
	toolbar_preview_->setEnabled(false);
}

/**
 * @brief Event handler for the "View::Log" menu: open the Logger window.
 */
void MainWindow::viewLogger()
{
	logger_.show(); //the logger is always kept in scope
	logger_.raise(); //bring to front
}

/**
 * @brief Event listener for when the program is closed.
 * @details This function checks whether there are unsaved changes before exiting the program,
 * and if yes, offers the usual options.
 * @param[in] event The received close event.
 */
void MainWindow::closeEvent(QCloseEvent *event)
{ //on macOS, this is not called on 'Quit'
	const bool perform_close = closeIni();
	if (perform_close)
		event->accept();
	else
		event->ignore();
}

/**
 * @brief Event handler for the "View::Settings" menu: open the settings tab.
 */
void MainWindow::viewSettings()
{
	//so the settings window has most current values (restore to this size after "appearance" re-start):
	setWindowSizeSettings();
	settings_window_->loadSettings();
}

/**
 * @brief Toggle showing the workflow panel.
 */
void MainWindow::toggleWorkflow()
{
	const QList<int> sizes( control_panel_->getSplitterSizes());
	if (sizes.at(0) < 20) { //show
		control_panel_->setSplitterSizes(QList<int>( ), true);
	} else { //hide
		setSplitterSizeSettings();
		control_panel_->setSplitterSizes(QList<int>( {0, this->width()} ));
	}
}

/**
 * @brief Display the 'About' window.
 */
void MainWindow::helpAbout()
{
	static auto *about = new AboutWindow(this);
	about->show();
	about->raise();
}

/**
 * @brief Delegate the toolbar button clicks to the appropriate calls.
 * @param[in] function Tag name for the function to perform.
 */
void MainWindow::toolbarClick(const QString &function)
{
	if (function == "save_ini")
		(void) saveIni();
	else if (function =="save_ini_as")
		saveIniAs();
	else if (function == "open_ini")
		openIni();
	else if (function == "reset_gui") //reset to default
		clearGui();
	else if (function == "clear_gui") //reset to nothing
		clearGui(false); //not actually a toolbar button but a menu
	else if (function == "preview")
		viewPreview();
}

/**
 * @brief Event listener for when the 'autoload' checkbox is clicked.
 * @details This function sets settings for the INI files to be automatically loaded for certain XMLs.
 * They are saved when the program quits.
 * @param[in] state The checkbox state (checked/unchecked).
 */
void MainWindow::onAutoloadCheck(const int &state)
{
	//Run through <ini> tags in appropriate settings file section and look for the current
	//application. If it is found, when the checkbox is checked it will be set to the current
	//INI file. If the box is unchecked the entry is deleted:
	QDomNode autoload_node( global_xml_settings.firstChildElement().firstChildElement("user").firstChildElement("autoload") );
	for (auto ini = autoload_node.firstChildElement("ini"); !ini.isNull(); ini = ini.nextSiblingElement("ini")) {
		if (ini.attribute("application").toLower() == current_application_.toLower()) {
			if (state == Qt::Checked)
				ini.firstChild().setNodeValue(ini_filename_->text());
			else
				ini.parentNode().removeChild(ini);
			return;
		}
	}

	//not found - create new settings node if user is enabling:
	if (state == Qt::Checked) {
		QDomNode ini( autoload_node.appendChild(global_xml_settings.createElement("ini")) );
		ini.toElement().setAttribute("application", current_application_);
		ini.appendChild(global_xml_settings.createTextNode(ini_filename_->text()));
	}
}

/**
 * @brief Event listener for requests to show the context menu for our toolbar.
 */
void MainWindow::onToolbarContextMenuRequest(const QPoint &/*pos*/)
{
	const QAction *selected( toolbar_context_menu_.exec(QCursor::pos()) ); //show at cursor position
	if (selected) { //an item of the menu was clicked
		if (selected->text() == (tr("Fix toolbar position"))) {
			toolbar_->setMovable(!selected->isChecked());
			setSetting("user::appearance::fix_toolbar_pos", "value",
			    selected->isChecked()? "TRUE" : "FALSE");
		}
	}
}

/**
 * @brief Helper function to write file info only once if something goes wrong.
 * @param[in] first_error_message Boolean that is set to true for the current run
 * until this function is called the first time.
 * @param[in] ini INIParser that is currently processing
 */
void MainWindow::writeGuiFromIniHeader(bool &first_error_message, const INIParser &ini)
{
	if (first_error_message) {
		logger_.log(tr(R"(Loading INI file "%1" into %2...)").arg(
		    ini.getFilename(), current_application_));
		first_error_message = false;
	}
}

#ifdef DEBUG
/**
 * @brief In debug mode add a menu with some helpful debugging functions.
 */
void MainWindow::createDebugMenu()
{
	/* DEBUG menu */
	auto *menu_debug = this->menuBar()->addMenu("&Debug");
	/* count menu */
	auto *menu_count = new QMenu("&Count", menu_debug);
	menu_debug->addMenu(menu_count);
	auto *count_panels = new QAction("&Panels", menu_count);
	menu_count->addAction(count_panels);
	connect(count_panels, &QAction::triggered, this, [=] {
		auto panel_list( control_panel_->getSectionTab()->findChildren<Atomic *>() );
		int input_panels = 0;
		for (auto &pan : panel_list) {
			if (!pan->property("no_ini").toBool()) //exclude helptexts, labels, grids
				input_panels++;
		}
		std::cout << input_panels << " input panels (" << panel_list.count() << " total elements)" << std::endl;
	});
	auto *count_replicators = new QAction("&Replicators", menu_count);
	menu_count->addAction(count_replicators);
	connect(count_replicators, &QAction::triggered, this, [=] {
		auto rep_list( control_panel_->getSectionTab()->findChildren<Replicator *>() );
		std::cout << rep_list.count() << R"( panels of type "Replicator")" << std::endl;
	});
	menu_debug->addSeparator();
	/* run action */
	auto *debug_run = new QAction("Run &action", menu_debug); //to run arbitrary code with a click
	menu_debug->addAction(debug_run);
	connect(debug_run, &QAction::triggered, this, &MainWindow::debugRunClick);
}


#endif //def DEBUG

#ifdef DEBUG
/**
 * @brief This is a dummy menu entry to execute arbitrary test code from.
 */
void MainWindow::debugRunClick()
{
    setStatus("Debug menu clicked", "warning", false, 5000);
}
#endif //def DEBUG
