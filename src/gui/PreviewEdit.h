//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2020 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * The PreviewWindow uses this PreviewEdit for INI text editor features.
 * 2020-03
 */

#ifndef PREVIEWEDIT_H
#define PREVIEWEDIT_H

#include <src/main/SyntaxHighlighter.h>

#include <QAction>
#include <QString>
#include <QTextDocument>
#include <QPlainTextEdit>
#include <QWidget>

class PreviewEdit : public QPlainTextEdit
{
	Q_OBJECT

	public:
		PreviewEdit(const bool &monospace = true, const bool &typesetting = true,
			QWidget *parent = nullptr);

		void repaintSidePanel(QPaintEvent *event);
		int getSidePanelWidth();
		QString getFileName() const noexcept { return file_name_; }
		QString getFilePath() const noexcept { return file_path_; }
		QString getFullFilePath() const;
		void setFile(const QString &file_path); //call with full file path
		void setSyntaxHighlighting(const bool &enabled);

	protected:
		void setMonospacedFont();
		void resizeEvent(QResizeEvent *event) override;
		void dragMoveEvent(QDragMoveEvent *event) override;

	private slots:
		void updateSidePanelWidth();
		void updateSidePanel(const QRect &, int);

	private:
		QWidget *sidePanel = nullptr;
		QString file_path_; //file path without name
		QString file_name_;
		SyntaxHighlighter *highlighter_ = nullptr;
		const int margin_width = fontMetrics().boundingRect(QLatin1Char('M')).width();
};


class PreviewSidePanel : public QWidget
{
	Q_OBJECT

	public:
		PreviewSidePanel(PreviewEdit *editor) : QWidget(editor), textEdit(editor) {}

		QSize sizeHint() const { return QSize(textEdit->getSidePanelWidth(), 0); }

	protected:
		void paintEvent(QPaintEvent *event) { textEdit->repaintSidePanel(event); }

	private:
		PreviewEdit *textEdit;
};

#endif //PREVIEWEDIT_H
