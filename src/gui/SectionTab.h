//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2021 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * This is the high level tab bar widget which the GUI building recursion can be
 * started on.
 * Hence, an instance of it is used in the MainPanel, the Help and Settings
 * windows, and potentially others to display INIshell XML content.
 * 2021-01
 */

#ifndef SECTIONTAB_H
#define SECTIONTAB_H

#include <src/main/INIParser.h>
#include <src/panels/Group.h>

#include <QList>
#include <QScrollArea>
#include <QString>
#include <QStringList>
#include <QTabWidget>
#include <QWidget>
#include <QtXml>

class ScrollPanel : public QWidget {
	Q_OBJECT

	public:
		explicit ScrollPanel(const QString &tab_color, QWidget *parent = nullptr);
		Group * getGroup() const;
		void ensureWidgetVisible(QWidget *wid) { main_area_->ensureWidgetVisible(wid); }

	private:
		QScrollArea *main_area_ = nullptr;
		Group *main_group_ = nullptr;
};

class SectionTab : public QTabWidget {
	Q_OBJECT

	public:
		explicit SectionTab(QWidget *parent = nullptr);
		ScrollPanel * getSectionScrollArea(const QString &section, const QString &background_color = QString(),
			const QString &color = "normal", const bool &no_create = false, const int &insert_idx = -1);
		ScrollPanel * getSectionScrollArea(const int &index);
		QString setIniValuesFromGui(INIParser *ini, const bool &insert_missing = false);
		void setSectionDynamic(const QString &section, const QDomNode &node); //register section as dynamic
		bool isDynamicParent(const QString &section) const;
		int isDynamicChild(const QString &section) const;
		bool isDynamicChildOf(const QString &section, const QString &parent_section) const;
		int countChildren(const QString &parent_section) const;
		QString getParentOf(const QString &section);
		void spawnDynamicSection(const QString &section, const QString &new_name = QString()); //clone a section registered as dynamic
		ScrollPanel * tryDynamicSection(const QString &section);
		void removeDynamicSection(const QString &section);
		QStringList * getDynamicSectionList() { return &dynamic_sections_; }
		bool showTab(const QString &tab_name);
		bool showPanel(const QString &section, const QString &element_key, const bool &no_create = false);
		bool isPanelVisible(const Atomic &panel, const Qt::CaseSensitivity &sensitivity = Qt::CaseInsensitive) const;
		int getIndex(const QString &section, const Qt::CaseSensitivity &sensitivity = Qt::CaseInsensitive) const;
		void clear();
		void clearDynamicTabs();

	private:
		QStringList dynamic_sections_;
		QList<QDomNode> dynamic_nodes_;
		QList<int> dynamic_running_indices_;
};

#endif //SECTIONTAB_H
