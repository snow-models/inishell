//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2021 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Window to display and handle the INIshell settings XML file.
 * 2021-01
 */

#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <src/gui/SectionTab.h>
#include <src/main/INIParser.h>

#include <QCloseEvent>
#include <QMainWindow>

class SettingsWindow : public QMainWindow {
	Q_OBJECT

	public:
		explicit SettingsWindow(QMainWindow *parent = nullptr);
		~SettingsWindow() override;
		void loadSettings();
		SectionTab * getSectionTab() const noexcept { return settings_tab_; }

	protected:
		void keyPressEvent(QKeyEvent *event) override;

	private:
		QString getShellSetting(QWidget *parent, const QString &option);
		void displaySettings();
		void setSettings();
		int appearanceWarnBox();

		SectionTab *settings_tab_ = nullptr;
		INIParser *orig_settings_ini_ = nullptr;
		bool settings_are_loaded_ = false;

	private slots:
		void clearSettings();
};

#endif //SETTINGSWINDOW_H
