//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2021 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Show a warning dialog with a checkbox that can be ticked to not show this message again.
 * 2021-01
 */

#ifndef REMEMBERDIALOG_H
#define REMEMBERDIALOG_H

#include <QMessageBox>

class RememberDialog : public QMessageBox {
	Q_OBJECT

	public:
		explicit RememberDialog(const QString &setting, const QString &message, const QString &informative = QString(),
			const QString &details = QString(),
			const QFlags<QMessageBox::StandardButton> &buttons = (QMessageBox::Ok | QMessageBox::Cancel),
			QWidget *parent = nullptr);
};


#endif //REMEMBERDIALOG_H
