//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2019 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * The main information grouping element including the Workflow panel and main tab panel.
 * It is the central widget of the main window and is size-controlled by it.
 * All main content goes here.
 * 2019-10
 */

#ifndef MAINPANEL_H
#define MAINPANEL_H

#include <src/gui/SectionTab.h>
#include <src/gui/WorkflowPanel.h>
#include <src/panels/panels.h>

#include <QList>
#include <QStackedWidget>
#include <QSplitter>
#include <QString>
#include <QStringList>
#include <QTabWidget>
#include <QWidget>
#include <QtXml>

class MainPanel : public QWidget {
	Q_OBJECT

	public:
		explicit MainPanel(QWidget *parent = nullptr);
		WorkflowPanel * getWorkflowPanel() const { return workflow_panel_; }
		QStackedWidget * getWorkflowStack() const { return workflow_stack_; }
		QString setIniValuesFromGui(INIParser *ini);
		void displayInfo();
		QList<int> getSplitterSizes() const;
		void setSplitterSizes(QList<int> sizes = QList<int>(), bool enforce_visible = false);
		void clearGuiElements();
		void clearGui(const bool &set_default = true);
		SectionTab * getSectionTab() const noexcept { return section_tab_; }

	private:
		WorkflowPanel *workflow_panel_ = nullptr;
		QStackedWidget *workflow_stack_ = nullptr;
		SectionTab *section_tab_ = nullptr;
		QSplitter *splitter_ = nullptr;
};

#endif //MAINPANEL_H
