//SPDX-License-Identifier: GPL-3.0-or-later
/*****************************************************************************/
/*  Copyright 2021 WSL Institute for Snow and Avalanche Research  SLF-DAVOS  */
/*****************************************************************************/
/* This file is part of INIshell.
   INIshell is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   INIshell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with INIshell.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <src/gui/RememberDialog.h>
#include <src/main/colors.h>
#include <src/main/settings.h>

#include <QCheckBox>
#include <QCoreApplication>

/**
 * @class RememberDialog
 * @brief A warning dialog with a "do not show again" checkbox.
 * @param[in] setting The setting name (of INIshell's global settings) to use for the checkbox value.
 * @param[in] message The warning message to display.
 * @param[in] informative Additional information text to show.
 * @param[in] details Additional details to show.
 * @param[in] buttons The button set to use and return the value of on exec().
 * @param[in] parent The parent widget.
 */
RememberDialog::RememberDialog(const QString &setting, const QString &message, const QString &informative,
	const QString &details, const QFlags<StandardButton> &buttons, QWidget *parent) : QMessageBox(parent)
{
	this->setWindowTitle("Warning ~ " + QCoreApplication::applicationName());
	this->setText("<b>" + message + "</b>");
	this->setInformativeText(informative);
	this->setDetailedText(details);
	this->setIcon(QMessageBox::Warning);
	this->setStandardButtons(buttons);
	this->setDefaultButton(QMessageBox::Cancel);

	auto *show_msg_again = new QCheckBox(tr("Don't show this warning again"), this);
	show_msg_again->setToolTip(tr("The warning can be re-enabled in the settings"));
	show_msg_again->setStyleSheet("QCheckBox {color: " + colors::getQColor("info").name() + "}");
	this->setCheckBox(show_msg_again);
	connect(show_msg_again, &QCheckBox::stateChanged, [setting](int state) {
		const bool checked = (state == Qt::CheckState::Checked);
		setSetting(setting, "value", !checked? "TRUE" : "FALSE");
	});
	//TODO: remember choice checkbox?
}
