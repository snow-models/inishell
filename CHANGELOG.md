# Changelog

Releases of the INIshell project will be documented in this file.

## [Unreleased]

- Nothing yet

## [2.0.8]

### Added

- Schema validation: The simulation software XMLs are now checked for their
  integrity before use with an advanced XSD schema validation ensuring
  correct semantics in all XMLs
- Data file preview: when selecing a file it can be explored in a quick to
  access and syntax highlighted preview window
- Support for connected path / file name input panels, i. e. different
  FilePath input panels can now communicate
- Support for collapsible frames (to show/hide a group of settings)
- Many new model configuration keys including cloudiness computation settings,
  several Alpine3d keys and the MySQL plugin
- Link to INIshell paper

### Changed

- Several overall UI enhancements

### Fixed

- Several smaller bugs

### Framework

- Major packaging enhancements
- Updated to Qt 6 with backwards compatibility to Qt 5
- Internal build and test pipeline is now operational
- Modernization of code and project hosting

## [2.0.7] - 2021-12-10

### Added

- Possibility for Replicators to have empty keys:
  They can now act as an interactive GUI element with no corresponding
  INI structure (for e. g. to intuitively group a batch of settings).
- Some debugging tools
- Dedicated help texts for Replicators
- Possibility in a Selector's help text to reference the parameter name
- "placeholder" texts in dropdown-mode Selectors
- DAME project XML
- Displaying link locations
- Many new model configuration parameters including the grid resampling section,
  the MeteoBlue plugin and more ACDD fields
- New panel: Copytexts (interactively display syntax highlighted pieces of text)
- New panel: ImageView (display images on the INIshell canvas)
- Support for xy() coordinates
- Workflow user input is now remembered per application
- XML dev tools: symbols & html entity tables, solarized color picker

### Changed

- Replicators can now have numbers anywhere, not just at the end
- Non-nested Replicators can now replace multiple '#'s
- Various smaller UI enhancements
- Automated "textmode" attribute for Selectors
- Some panels now have much better documentation
- Meteo param OSWR is now called RSWR in example simulation

### Fixed

- Bug reading back in the MATHS filter settings
- Selector tooltip now shows the key like the Replicator
- ToolTip help for Preview Editor shortcuts is now native
- Segfaults

### Framework

- Ported the project from qmake to CMake
- (Bundled) packaging now availale through CMake
- Complete rework of the Replicator panel's logic and documentation
- More generic syntax highlighting - can now be done anywhere

## [2.0.6] - 2020-03-01

This version adds support for dynamic sections and major UI enhancements,
especially in the main GUI area and the Help and Settings windows.

### Added

- Support for dynamic sections:
  Sections can now be replicated as Input1, Input2, ...
- Support for internal links in the XMLs:
  Hyperlinks can now open help topics and highlight input parameters
- "Duplicate", "Open parent folder" and "Delete" context menu for INI files
- Prompt to auto-restart Inishell after appearance settings change
- New model software features/options
- This changelog

### Changed

- Optional/mandatory highlighting is now done on the label
- (In)valid expressions are marked with little icons leading to the help files
- Help is now displayed in a separate browser
  (handling all help topics)
- Settings are now a separate window:
  Standard "reset", "save" and "clear" buttons (in addition to cmd line)
- Display sections for missing mandatory INI keys in message when saving
- Jump to 1st missing input value if saving is cancelled
- Workflow panel is now toggled instead of separate show/hide menus
- Always use/show '.' as decimal separator

### Fixed

- MacOS help and other menus
- Replicator default value labelling
- "Unsaved" marker bug in the Preview Editor
- Edge case crash of the help file
- Context help works reliably now
- Several macOS integration improvements
- Several overall UI improvements
- Selectors and Replicators are not reported missing if they have children
- Performance improvements
- Fixed unit test
- Transportation of unknown keys to preview and output
- Example simulation paths with new working directory field
- Insert missing and mandatory keys in Preview Editor
- Several minor bugs

### Framework

- Easy-to-use subclassed TabBar which is now the main recursion container
  (therefore XMLs can now be transparently loaded into any area we'd like)
- Easy usage of "do not show again" dialogs now possible
- Major refactoring and logic improvements in MainWindow and MainPanel
- "Highlight" functionality for all input panels

## [2.0.5] - 2020-12-16

This version adds current features to the XMLs and fixes some bugs.

### Added

- New XML features
- Archive manifest file

### Fixed

- Workflow and path fixes

## [2.0.4] - 2020-10-01

This version prepares an Alpine3d release.

### Added

- New MeteoIO/Snowpack features

### Fixed

- Improvements to the build process

## [2.0.3] - 2020-09-01

This version introduces a new panel and improves on the XMLs and
user interface.

### Added

- Date/time picker panel
- PreviewEditor functionality
- Most remaining features are now described in the XMLs

### Fixed

- Improvements to the build process
- Improvements of the user interface

## [2.0.3beta] - 2020-05-27

This version provides an Alpine3d Inishell file.

### Added

- Alpine3d Inishell file
- TechSnow module

### Fixed

- Minor cosmetic fixes

## [2.0.2beta] - 2020-05-04

This version improves multi platform support, builds upon and enhances
recently added features, adds functionality to the XMLs themselves and fixes
several smaller issues.

### Added

- Text conversion features for the Preview Editor
- INI file features for the Preview Editor
- "filename" mode in FilePath
- Line numbering in Preview
- Numerous XML descriptions of features

### Changed

- Better handling of expressions
- Better icons

### Fixed

- Numerous packaging issues
- Number panel precision issues
- Integration improvements for macOS
- Serveral overall fixes

## [2.0.1beta] - 2020-03-11

First public beta release of INIshell v2 (inishell-ng).
The program is ready to be tried out by peers and practitioners.

[unreleased]: https://code.wsl.ch/snow-models/inishell/-/tree/master
[2.0.8]: https://code.wsl.ch/snow-models/inishell/-/tree/v2.0.8
[2.0.7]: https://code.wsl.ch/snow-models/inishell/-/tree/v2.0.7
[2.0.6]: https://code.wsl.ch/snow-models/inishell/-/tree/v2.0.6
[2.0.5]: https://code.wsl.ch/snow-models/inishell/-/tree/v2.0.5
[2.0.4]: https://code.wsl.ch/snow-models/inishell/-/tree/v2.0.4
[2.0.3]: https://code.wsl.ch/snow-models/inishell/-/tree/v2.0.3
[2.0.3beta]: https://code.wsl.ch/snow-models/inishell/-/tree/v2.0.3beta
[2.0.2beta]: https://code.wsl.ch/snow-models/inishell/-/tree/v2.0.2beta
[2.0.1beta]: https://code.wsl.ch/snow-models/inishell/-/tree/v2.0.1beta
