#!/bin/bash

#Run INIshell unit tests.
#https://gitlabext.wsl.ch/snow-models/inishell

../bin/inishell --version
echo -e "*** Starting INIshell. This is its output:\n"
../bin/inishell -i unit_test_inireader.ini -o unit_test_inireader_output.ini AAA::delete="" NEW::new_key=new_value --exit

difference=$(diff "unit_test_inireader_output.ini" "unit_test_inireader_expected_result.ini")
return_code=$(echo "$?")
if [ -z "$difference" ]; then
      echo  "*** All ok for the INI parser."
else
      echo "[E] The ini reader did not produce the expected result! Difference:"
      echo "$difference"
      echo "[i] NOTE: Make sure you have set whitespace handling to 'USER' in INIshell's settings!"
fi

echo "*** All done"
exit $return_code