Welcome to Inishell, a flexible Graphical User Interface (GUI) for simulation software.
Inishell is open source under the GNU GPL3 license and is shipped as the GUI for various numerical modelling software packages to configure and run the simulations intuitively.

Inishell loads XML files containing a semantic description of the models' parameters and dynamically builds a GUI upon this information.
Hence, for the end user it offers a graphical user interface for provided modeling software which is tailored to rapid development and therefore hopefully never outdated. For the developer on the other hand, it offers the possibility to very easily set up a GUI for their application and keep it synchronized with the evolving models. Deploying a new feature in the GUI is then as easy as editing an XML text file and shipping this updated version to the user.
