INIshell
====================

Introduction
--------------------

Welcome to Inishell, a flexible Graphical User Interface (GUI) for simulation software.
Inishell is open source under the GNU GPL3 license and is shipped as the GUI for various numerical modelling software packages to configure and run the simulations intuitively.

Inishell loads XML files containing a semantic description of the models' parameters and dynamically builds a GUI upon this information.
Hence, for the end user it offers a graphical user interface for provided modeling software which is tailored to rapid development and therefore hopefully never outdated. For the developer on the other hand, it offers the possibility to very easily set up a GUI for their application and keep it synchronized with the evolving models. Deploying a new feature in the GUI is then as easy as editing an XML text file and shipping this updated version to the user. 

You can find more details in the following paper: Bavay, M., Reisecker, M., Egger, T., and Korhammer, D.: *Inishell 2.0: semantically driven automatic GUI generation for scientific models*, Geosci. Model Dev., **15**, 365–378, https://doi.org/10.5194/gmd-15-365-2022, 2022.

- For help concerning the installation of Inishell as well as general information please visit Inishell's project page at https://inishell.slf.ch
- Once Inishell is running, it offers an extensive built-in help browser for both end users and developers.
- For technical information about the architecture and principles and how Inishell can help you as a developer please consult the paper referenced above.
- For help about the software that is being configured through Inishell (i. e. the numerical model itself), see the respective vendor's documentation (and of course the help that is provided for it through Inishell).
