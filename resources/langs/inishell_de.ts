<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AboutWindow</name>
    <message>
        <location filename="../../src/gui/AboutWindow.cc" line="62"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
</context>
<context>
    <name>ApplicationsView</name>
    <message>
        <location filename="../../src/gui/ApplicationsView.cc" line="57"/>
        <source>List of your applications/simulations.
Double-click to open, right-click for more options.</source>
        <translation>Liste der Anwendungen/Simulationen.
Doppelklick um zu öffnen, Rechtsklick für weitere Optionen.</translation>
    </message>
    <message>
        <location filename="../../src/gui/ApplicationsView.cc" line="111"/>
        <location filename="../../src/gui/ApplicationsView.cc" line="137"/>
        <source>Refresh</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <location filename="../../src/gui/ApplicationsView.cc" line="113"/>
        <source>Open in external editor</source>
        <translation>In externem Editor öffnen</translation>
    </message>
    <message>
        <location filename="../../src/gui/ApplicationsView.cc" line="114"/>
        <source>Append to current GUI</source>
        <translation>Zur momentanen Oberfläche hinzufügen</translation>
    </message>
    <message>
        <location filename="../../src/gui/ApplicationsView.cc" line="125"/>
        <source> for </source>
        <translation> für </translation>
    </message>
    <message>
        <location filename="../../src/gui/ApplicationsView.cc" line="140"/>
        <source>Append to current</source>
        <translation>Zur momentanen</translation>
    </message>
    <message>
        <location filename="../../src/gui/ApplicationsView.cc" line="145"/>
        <source>Open in external</source>
        <translation>In externem Editor</translation>
    </message>
</context>
<context>
    <name>Atomic</name>
    <message>
        <location filename="../../src/panels/Atomic.cc" line="446"/>
        <location filename="../../src/panels/Atomic.cc" line="470"/>
        <source>Reset to default</source>
        <translation>Auf Standardwert zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../src/panels/Atomic.cc" line="447"/>
        <location filename="../../src/panels/Atomic.cc" line="472"/>
        <source>Delete key</source>
        <translation>Schlüssel löschen</translation>
    </message>
</context>
<context>
    <name>Checkbox</name>
    <message>
        <location filename="../../src/panels/Checkbox.cc" line="104"/>
        <source>XML error: Ignored additional option in Checkbox &quot;%1::%2&quot;, there can only be a single one.</source>
        <translation>XML Fehler: Überflüssige Option ignoriert für die Checkbox &quot;%1::%2&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/panels/Checkbox.cc" line="128"/>
        <source>Ignored non-boolean value &quot;%1&quot; for checkbox &quot;%2::%3&quot;</source>
        <translation>Boolean-Wert &quot;%1&quot; wurde für die Checkbox &quot;%2::%3&quot; ignoriert</translation>
    </message>
</context>
<context>
    <name>Checklist</name>
    <message>
        <location filename="../../src/panels/Checklist.cc" line="127"/>
        <source>XML error: Invalid XML syntax for Checklist panel &quot;%1::%2&quot;: no checkable options set.</source>
        <translation>XML-Fehler: Invalide Syntax für Checklist &quot;%1::%2&quot;: keine Auswahlmöglichkeiten vorhanden.</translation>
    </message>
    <message>
        <location filename="../../src/panels/Checklist.cc" line="246"/>
        <source>Checklist item &quot;%1&quot; could not be set from INI file for key &quot;%2::%3&quot;: no such option specified in XML file</source>
        <translation>Der Checklist-Eintrag &quot;%1&quot; konnte für den INI-Schlüssel &quot;%2::%3&quot; nicht gesetzt werden: Option ist nicht in XML vorhanden.</translation>
    </message>
</context>
<context>
    <name>Choice</name>
    <message>
        <location filename="../../src/panels/Choice.cc" line="218"/>
        <source>XML error: No checkable options set for Choice panel &quot;%1::%2&quot;.</source>
        <translation>XML-Fehler: Keine Auswahlmöglichkeiten für Choice &quot;%1::%2&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/panels/Choice.cc" line="243"/>
        <source>Choice item \&quot;%1\&quot; could not be set from INI file for key &quot;%2::%3&quot;: no such option specified in XML file</source>
        <translation>Der Choice-Eintrag &quot;%1&quot; konnte für den INI-Schlüssel &quot;%2::%3&quot; nicht gesetzt werden: Option ist nicht in XML vorhanden.</translation>
    </message>
</context>
<context>
    <name>Copytext</name>
    <message>
        <location filename="../../src/panels/Copytext.cc" line="139"/>
        <source>XML error: Could not parse height for some &quot;Copytext&quot; panel (must be integral)</source>
        <translation>XML Fehler: Höhe eines &quot;Copytext&quot;-Feldes ist invalid (muss eine ganze Zahl sein)</translation>
    </message>
    <message>
        <location filename="../../src/panels/Copytext.cc" line="166"/>
        <source>Copied to clipboard</source>
        <translation>Kopiert</translation>
    </message>
</context>
<context>
    <name>Datepicker</name>
    <message>
        <location filename="../../src/panels/Datepicker.cc" line="75"/>
        <source>Pick a date/time</source>
        <translation>Datum/Zeit wählen</translation>
    </message>
</context>
<context>
    <name>Dropdown</name>
    <message>
        <location filename="../../src/panels/Dropdown.cc" line="136"/>
        <source>&lt;edit&gt;</source>
        <translation>&lt;bearbeiten&gt;</translation>
    </message>
    <message>
        <location filename="../../src/panels/Dropdown.cc" line="215"/>
        <source>XML error: Multiple default values given in option-attributes of Alternative panel &quot;%1::%2&quot;</source>
        <translation>XML-Fehler: Mehrere Default-Werte für das Alternative-Element &quot;%1::%2&quot; vorhanden</translation>
    </message>
    <message>
        <location filename="../../src/panels/Dropdown.cc" line="339"/>
        <source>Value &quot;%1&quot; could not be set in Alternative panel from INI file for key &quot;%2::%3&quot;: no such option specified in XML file</source>
        <translation>Der Alternative-Eintrag &quot;%1&quot; konnte für den INI-Schlüssel &quot;%2::%3&quot; nicht gesetzt werden: Option ist nicht in XML vorhanden.</translation>
    </message>
</context>
<context>
    <name>FilePath</name>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="278"/>
        <source>Show preview</source>
        <translation>Öffne Vorschau</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="319"/>
        <source>&lt;no path set&gt;</source>
        <translation>&lt;kein Pfad&gt;</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="319"/>
        <source>&lt;no file set&gt;</source>
        <translation>&lt;keine Datei&gt;</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="339"/>
        <source>Open path</source>
        <translation>Pfad öffnen</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="355"/>
        <source>Path of file linked to &quot;%1&quot;</source>
        <translation>Pfad der Datei ist mit &quot;%1&quot; verlinkt</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="369"/>
        <source>Open file</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="381"/>
        <source>All Files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="460"/>
        <source>[Empty file name]</source>
        <translation>[Leerer Dateiname]</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="465"/>
        <source>[Folder does not exist]</source>
        <translation>[Ordner existiert nicht]</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="465"/>
        <source>[File does not exist]</source>
        <translation>[Datei existiert nicht]</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="467"/>
        <source>[Directory path points to a file]</source>
        <translation>[Ordner-Pfad zeigt auf Datei]</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="469"/>
        <source>[File path points to a directory]</source>
        <translation>[Datei-Pfad zeigt auf Ordner]</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="471"/>
        <source>[File not readable for current user (owned by &quot;%1&quot;)]</source>
        <translation>[Unlesbar für diesen Benutzer (Besitzer: &quot;%1&quot;)]</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="474"/>
        <source>[File not writable for current user (owned by &quot;%1&quot;)]</source>
        <translation>[Nicht schreibbar für diesen Benutzer (Besitzer: &quot;%1&quot;)]</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="477"/>
        <source>[File not accessible for current user (owned by &quot;%1&quot;)]</source>
        <translation>[Kein Zugriff für diesen Benutzer (Beistzer: &quot;%1&quot;)]</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="480"/>
        <source>[File is an executable]</source>
        <translation>[Datei ist ein Programm]</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="482"/>
        <source>[File is a symbolic link]</source>
        <translation>[Datei ist eine Veknüpfung]</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="484"/>
        <source>[File already exists]</source>
        <translation>[Datei existiert bereits]</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="486"/>
        <source>[File name has leading or trailing whitespaces]</source>
        <translation>[Dateiname beinhaltet Leerzeichen am Anfang oder Ende]</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="528"/>
        <source>Open Folder</source>
        <translation>Ordner öffnen</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="532"/>
        <location filename="../../src/panels/FilePath.cc" line="535"/>
        <source>Open File</source>
        <translation>Datei öffnen</translation>
    </message>
</context>
<context>
    <name>FilePreview</name>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="57"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="115"/>
        <source>Error reading file: </source>
        <translation>Fehler beim Lesen der Datei: </translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="124"/>
        <location filename="../../src/panels/FilePath.cc" line="127"/>
        <source>File size is %1</source>
        <translation>Dateigröße: %1</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="129"/>
        <source>Do you really want to view this huge file?</source>
        <translation>Soll wirklich eine sehr große Datei angezeigt werden?</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="130"/>
        <source>It may be a good idea to disable syntax highlighting first, as this will slow down the read-in.</source>
        <translation>Vor dem Öffnen großer Dateien kann es hilfreich sein die Texthervorhebung auszuschalten, da diese den Lesevorgang verlangsamt.</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="162"/>
        <source>(truncated - right-click to show more)</source>
        <translation>(gekürzt - rechts-klicken um mehr der Datei zu zeigen)</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="177"/>
        <location filename="../../src/panels/FilePath.cc" line="211"/>
        <source>Open in INI Preview Editor</source>
        <translation>Im Vorschau-Editor öffnen</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="178"/>
        <location filename="../../src/panels/FilePath.cc" line="217"/>
        <source>Open in external editor</source>
        <translation>In externem Editor öffnen</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="179"/>
        <location filename="../../src/panels/FilePath.cc" line="220"/>
        <source>Syntax highlighting</source>
        <translation>Texthervorhebung</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="184"/>
        <location filename="../../src/panels/FilePath.cc" line="223"/>
        <source>Line wrapping</source>
        <translation>Zeilenumbrüche</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="190"/>
        <location filename="../../src/panels/FilePath.cc" line="196"/>
        <source>Show %1 lines</source>
        <translation>Zeige %1 Zeilen</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="201"/>
        <source>Show all data</source>
        <translation>Zeige alle Zeilen</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="207"/>
        <location filename="../../src/panels/FilePath.cc" line="232"/>
        <source>File info</source>
        <translation>Dateiinformation</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="235"/>
        <location filename="../../src/panels/FilePath.cc" line="239"/>
        <source>File size: %1</source>
        <translation>Dateigröße: %1</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="240"/>
        <source>Created on: %1</source>
        <translation>Erstellt am: %1</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="242"/>
        <source>

Path: %1</source>
        <translation>

Pfad: %1</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="243"/>
        <source>
Last modified: %1</source>
        <translation>
Zuletzt geändert am: %1</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="244"/>
        <source>
Owned by: %1</source>
        <translation>
Besitzer: %1</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="245"/>
        <source>
Writeable: %1</source>
        <translation>
Schreibbar: %1</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="245"/>
        <source>yes</source>
        <translation>ja</translation>
    </message>
    <message>
        <location filename="../../src/panels/FilePath.cc" line="245"/>
        <source>no</source>
        <translation>nein</translation>
    </message>
</context>
<context>
    <name>GridPanel</name>
    <message>
        <location filename="../../src/panels/GridPanel.cc" line="82"/>
        <source>XML error: Unsuitable rowspan or colspan argument in grid for key &quot;%1::%2&quot;</source>
        <translation>XML-Fehler: Invalides rowspan oder colspan Argument im Grid &quot;%1::%2&quot;</translation>
    </message>
    <message>
        <location filename="../../src/panels/GridPanel.cc" line="96"/>
        <source>XML error: Unsuitable or missing grid row or column index (both must be an integer equal or greater than 1) for key &quot;%1::%2&quot;</source>
        <translation>XML-Fehler: Invalider oder Fehlender index für eine Grid-Zeile oder -Spalte (beide müssen eine Zahl größer gleich 1 sein) für den Schlüssel &quot;%1::%2&quot;</translation>
    </message>
    <message>
        <location filename="../../src/panels/GridPanel.cc" line="103"/>
        <source>XML error: No child panels specified for Grid &quot;%1::%2&quot;</source>
        <translation>XML-Fehler: Keine Kind-Elemente im Grid &quot;%1::%2&quot;</translation>
    </message>
</context>
<context>
    <name>HelpWindow</name>
    <message>
        <location filename="../../src/gui/HelpWindow.cc" line="45"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../../src/gui/HelpWindow.cc" line="100"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../../src/gui/HelpWindow.cc" line="101"/>
        <source>&amp;Close help</source>
        <translation>Hilfe s&amp;chließen</translation>
    </message>
    <message>
        <location filename="../../src/gui/HelpWindow.cc" line="107"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="../../src/gui/HelpWindow.cc" line="108"/>
        <source>&amp;User guide</source>
        <translation>Ben&amp;utzerhandbuch</translation>
    </message>
    <message>
        <location filename="../../src/gui/HelpWindow.cc" line="114"/>
        <source>&amp;Developer&apos;s guide</source>
        <translation>Han&amp;dbuch für Entwickler</translation>
    </message>
    <message>
        <location filename="../../src/gui/HelpWindow.cc" line="121"/>
        <source>&amp;About</source>
        <translation>&amp;Über</translation>
    </message>
    <message>
        <location filename="../../src/gui/HelpWindow.cc" line="127"/>
        <source>View &amp;online help...</source>
        <translation>&amp;Online-Hilfe...</translation>
    </message>
    <message>
        <location filename="../../src/gui/HelpWindow.cc" line="131"/>
        <source>File &amp;bug report...</source>
        <translation>&amp;Fehler melden...</translation>
    </message>
    <message>
        <location filename="../../src/gui/HelpWindow.cc" line="135"/>
        <source>View Inishell &amp;paper...</source>
        <translation>Inishell-paper öffnen...</translation>
    </message>
</context>
<context>
    <name>HorizontalPanel</name>
    <message>
        <location filename="../../src/panels/HorizontalPanel.cc" line="74"/>
        <source>XML error: No child panels specified for Grid &quot;%1::%2&quot;</source>
        <translation>XML-Fehler: Keine Kind-Elemente für Grid &quot;%1::%2&quot;</translation>
    </message>
</context>
<context>
    <name>INIParser</name>
    <message>
        <location filename="../../src/main/INIParser.cc" line="391"/>
        <source>Different number of sections (%1 vs. %2).
This usually implies a different number of keys.

</source>
        <translation>Unterschiedliche Anzahl an Sektionen (%1 vs. %2)
Üblicherweise bedeuted dies eine unterschiedliche Anzahl an Schlüsseln.

</translation>
    </message>
    <message>
        <location filename="../../src/main/INIParser.cc" line="415"/>
        <source>Section &quot;%1&quot; not found</source>
        <translation>Sektion &quot;%1&quot; nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../src/main/INIParser.cc" line="421"/>
        <source>Different number of key/value pairs (%1 vs. %2)</source>
        <translation>Unterschiedliche Anzahl an Schlüssel/Werte-Paaren (%1 vs. %2)</translation>
    </message>
    <message>
        <location filename="../../src/main/INIParser.cc" line="427"/>
        <source>Key &quot;%1&quot; not found</source>
        <translation>Schlüssel &quot;%1&quot; nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../src/main/INIParser.cc" line="435"/>
        <source>(One of) the different key(s) is: &quot;%1&quot;</source>
        <translation>Unterschiedliche(r) INI-Schlüssel: &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../src/main/INIParser.cc" line="461"/>
        <source>Could not open INI file for reading</source>
        <translation>INI-Datei konnte nicht gelesen werden</translation>
    </message>
    <message>
        <location filename="../../src/main/INIParser.cc" line="727"/>
        <source>Undefined format on line %1 of file &quot;%2&quot;</source>
        <translation>Unbekanntes Format in Zeile %1 der Datei &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../../src/main/INIParser.cc" line="730"/>
        <source>Invalid line in file &quot;</source>
        <translation>Unbekanntes Format in Datei &quot;</translation>
    </message>
    <message>
        <location filename="../../src/main/INIParser.cc" line="839"/>
        <source>Reading INI file &quot;%1&quot;...</source>
        <translation>Lese INI-Datei &quot;%1&quot;...</translation>
    </message>
    <message>
        <location filename="../../src/main/INIParser.cc" line="617"/>
        <source>Could not open INI file for writing</source>
        <translation>INI-Datei konnte nicht geschrieben werden</translation>
    </message>
</context>
<context>
    <name>ImageView</name>
    <message>
        <location filename="../../src/panels/ImageView.cc" line="123"/>
        <source>Could not read image &quot;%1&quot;.</source>
        <translation>Konnte Bild &quot;%1&quot; nicht lesen.</translation>
    </message>
</context>
<context>
    <name>IniFolderView</name>
    <message>
        <location filename="../../src/gui/IniFolderView.cc" line="85"/>
        <source>INI files on your computer.
Double-click to open, right-click for more options.</source>
        <translation>INI-Dateien auf dem Computer.
Ein Doppelcklick öffnet die Datei; Rechtsklick für mehr Optionen.</translation>
    </message>
    <message>
        <location filename="../../src/gui/IniFolderView.cc" line="102"/>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location filename="../../src/gui/IniFolderView.cc" line="108"/>
        <source>Parent folder</source>
        <translation>Übergeordneter Pfad</translation>
    </message>
    <message>
        <location filename="../../src/gui/IniFolderView.cc" line="114"/>
        <source>Home directory</source>
        <translation>Heimverzeichnis</translation>
    </message>
    <message>
        <location filename="../../src/gui/IniFolderView.cc" line="120"/>
        <source>Working directory</source>
        <translation>Arbeitsverzeichnis</translation>
    </message>
    <message>
        <location filename="../../src/gui/IniFolderView.cc" line="176"/>
        <source>Open in external editor</source>
        <translation>In externem Editor öffnen</translation>
    </message>
    <message>
        <location filename="../../src/gui/IniFolderView.cc" line="179"/>
        <source>Load on top of current INI values</source>
        <translation>Zu momentanen INI-Werten hinzufügen</translation>
    </message>
    <message>
        <location filename="../../src/gui/IniFolderView.cc" line="181"/>
        <location filename="../../src/gui/IniFolderView.cc" line="226"/>
        <source>Duplicate</source>
        <translation>Duplizieren</translation>
    </message>
    <message>
        <location filename="../../src/gui/IniFolderView.cc" line="182"/>
        <location filename="../../src/gui/IniFolderView.cc" line="244"/>
        <source>Open parent folder</source>
        <translation>Übergeordneten Ordner öffnen</translation>
    </message>
    <message>
        <location filename="../../src/gui/IniFolderView.cc" line="183"/>
        <location filename="../../src/gui/IniFolderView.cc" line="233"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../../src/gui/IniFolderView.cc" line="221"/>
        <source>Open in external</source>
        <translation>In externem Editor</translation>
    </message>
    <message>
        <location filename="../../src/gui/IniFolderView.cc" line="224"/>
        <source>Load on top</source>
        <translation>Zu momentanen</translation>
    </message>
    <message>
        <location filename="../../src/gui/IniFolderView.cc" line="231"/>
        <source>(copy).</source>
        <translation>(Kopie).</translation>
    </message>
    <message>
        <location filename="../../src/gui/IniFolderView.cc" line="236"/>
        <source>&lt;b&gt;Delete file &quot;%1&quot;?&lt;/b&gt;</source>
        <translation>&lt;b&gt;Lösche Datei &quot;%1&quot;?&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>Logger</name>
    <message>
        <location filename="../../src/gui/Logger.cc" line="48"/>
        <source>&amp;Close</source>
        <translation>S&amp;chliessen</translation>
    </message>
    <message>
        <location filename="../../src/gui/Logger.cc" line="51"/>
        <source>&amp;Save as...</source>
        <translation>&amp;Speichern unter...</translation>
    </message>
    <message>
        <location filename="../../src/gui/Logger.cc" line="54"/>
        <source>C&amp;lear</source>
        <translation>&amp;Löschen</translation>
    </message>
    <message>
        <location filename="../../src/gui/Logger.cc" line="80"/>
        <source>Log Messages</source>
        <translation>Log-Nachrichten</translation>
    </message>
    <message>
        <location filename="../../src/gui/Logger.cc" line="106"/>
        <source>Running on </source>
        <translation>Betriebssystem: </translation>
    </message>
    <message>
        <location filename="../../src/gui/Logger.cc" line="107"/>
        <source>; built with Qt </source>
        <translation>; entwickelt mit Qt </translation>
    </message>
    <message>
        <location filename="../../src/gui/Logger.cc" line="132"/>
        <source>Save Log</source>
        <translation>Log Speichern</translation>
    </message>
    <message>
        <location filename="../../src/gui/Logger.cc" line="133"/>
        <source>Text Files</source>
        <translation>Textdateien</translation>
    </message>
    <message>
        <location filename="../../src/gui/Logger.cc" line="133"/>
        <source>All Files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="../../src/gui/Logger.cc" line="133"/>
        <source>HTML files</source>
        <translation>HTML-Dateien</translation>
    </message>
    <message>
        <location filename="../../src/gui/Logger.cc" line="140"/>
        <source>Could not open file for writing</source>
        <translation>Datei konnte nicht geschrieben werden</translation>
    </message>
</context>
<context>
    <name>MainPanel</name>
    <message>
        <location filename="../../src/gui/MainPanel.cc" line="74"/>
        <source>&lt;br&gt; 	    &amp;nbsp;&amp;nbsp;Welcome to &lt;b&gt;INIshell&lt;/b&gt;, a dynamic graphical user interface builder to manage INI files.&lt;br&gt; 	    &amp;nbsp;&amp;nbsp;Double-click an application to the left to begin.&lt;br&gt;&lt;br&gt; 		&amp;nbsp;&amp;nbsp;For help, click &quot;Help&quot; in the menu and visit &lt;a href=&quot;https://inishell.slf.ch&quot;&gt;INIshell&apos;s project page&lt;/a&gt;.&lt;br&gt; 		&amp;nbsp;&amp;nbsp;There, you will also find the well-documented &lt;a href=&quot;https://code.wsl.ch/snow-models/inishell/-/tree/master&quot;&gt;source code&lt;/a&gt;.&lt;br&gt; 	    &amp;nbsp;&amp;nbsp;If you don&apos;t know where to begin, press %1. 	</source>
        <translation>&lt;br&gt; 	    &amp;nbsp;&amp;nbsp;Willkommen bei &lt;b&gt;INIshell&lt;/b&gt;, einer dynamisch generierten Oberfläche um INI-Dateien zu verwalten.&lt;br&gt; 	    &amp;nbsp;&amp;nbsp;Ein Doppelklick auf eine Anwendung öffnet deren Einstellungen.&lt;br&gt;&lt;br&gt; 		&amp;nbsp;&amp;nbsp;Hilfe erhält man im &quot;Hilfe&quot;-Menü und auf &lt;a href=&quot;https://gitlabext.wsl.ch/snow-models/inishell&quot;&gt;INIshell&apos;s Projektseite&lt;/a&gt;.&lt;br&gt; 		&amp;nbsp;&amp;nbsp;Dort ist auch der gut kommentierte &lt;a href=&quot;https://gitlabext.wsl.ch/snow-models/inishell/-/tree/master&quot;&gt;Quellcode&lt;/a&gt; verfügbar.&lt;br&gt; 	    &amp;nbsp;&amp;nbsp;Drücken Sie %1 für weitere Informationen. 	</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="244"/>
        <source>&lt;b&gt;Missing mandatory INI values.&lt;/b&gt;</source>
        <translation>&lt;b&gt;Zwingend erforderliche INI-Schlüssel fehlen.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="245"/>
        <source>Some non-optional INI keys are not set.
See details for a list or go back to the GUI and set all highlighted fields.</source>
        <translation>Einige zwingende INI-Schlüssel sind nicht gesetzt.\nSiehe Details für eine Liste der Schlüssel, oder gehe zurück und setze alle markierten Felder.</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="247"/>
        <source>Missing INI keys:
</source>
        <translation>Fehlende INI-Schlüssel:
</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="261"/>
        <source>Saved to </source>
        <translation>Gespeichert als </translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="278"/>
        <source>Save INI file</source>
        <translation>INI-Datei speichern</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="304"/>
        <location filename="../../src/gui/MainWindow.cc" line="830"/>
        <source>Open INI file</source>
        <translation>INI-Datei öffnen</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="357"/>
        <source>%1 does not know INI section &quot;[%2]&quot;</source>
        <translation>%1 ist die INI-Sektion &quot;[%2]&quot; nicht bekannt</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="395"/>
        <source>Reading INI file...</source>
        <translation>INI-Datei wird gelesen...</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="409"/>
        <source>INI file read with unknown keys</source>
        <translation>INI-Datei mit unbekannten Schlüsseln gelesen</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="411"/>
        <source>INI file read </source>
        <translation>INI-Datei </translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="411"/>
        <source>successfully</source>
        <translation>mit Erfolg gelesen</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="411"/>
        <source>with warnings</source>
        <translation>mit Warnungen gelesen</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="417"/>
        <location filename="../../src/gui/MainWindow.cc" line="627"/>
        <source>autoload this INI for </source>
        <translation>öffne diese INI automatisch für </translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="450"/>
        <source>INI settings will be lost</source>
        <translation>INI-Einstellungen gehen verloren</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="452"/>
        <source>INI settings not saved yet</source>
        <translation>INI-Einstellungen noch nicht gespeichert</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="732"/>
        <source>&amp;Reset to default values</source>
        <translation>Auf Standa&amp;rdwerte zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="737"/>
        <source>&amp;Clear</source>
        <translation>Alles lös&amp;chen</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="758"/>
        <source>Toggle wor&amp;kflow</source>
        <translation>Wor&amp;kflow zeigen/verbergen</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="762"/>
        <source>&amp;Refresh applications</source>
        <translation>Applikationen aktualisie&amp;ren</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="1124"/>
        <source>Loading INI file &quot;%1&quot; into %2...</source>
        <translation>Lade INI-Datei &quot;%1&quot; in %2...</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="190"/>
        <source>Ready.</source>
        <translation>Bereit.</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="192"/>
        <source>Errors occurred on startup</source>
        <translation>Beim Programmstart traten Fehler auf</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="347"/>
        <source>%1 does not know INI key &quot;</source>
        <translation>Unbekannter %1 INI-Schlüssel </translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="538"/>
        <location filename="../../src/gui/MainWindow.cc" line="1107"/>
        <source>Fix toolbar position</source>
        <translation>Position der Werkzeugleiste fixieren</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="592"/>
        <source>Reading application XML...</source>
        <translation>Anwendungs-XML wird gelesen...</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="599"/>
        <source>Errors occured when parsing the XML configuration file</source>
        <translation>Beim Lesen der XML-Einstellungsdatei sind Fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="600"/>
        <source>File: &quot;</source>
        <translation>Datei: &quot;</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="614"/>
        <source>An application or simulation file that has previously been found is now missing. Right-click the list to refresh.</source>
        <translation>Eine zuvor gefundene Anwendungs- oder Simulations-Konfigurationsdatei ist nicht mehr vorhanden. Rechtsklick auf die Liste um zu Aktualisieren.</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="616"/>
        <source>File has been removed</source>
        <translation>Datei wurde gelöscht</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="708"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="709"/>
        <source>&amp;Open INI file...</source>
        <translation>INI-Datei &amp;öffnen...</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="713"/>
        <source>&amp;Save INI file</source>
        <translation>INI-Datei &amp;speichern</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="718"/>
        <source>Save INI file &amp;as...</source>
        <translation>INI-Datei speichern &amp;als...</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="724"/>
        <source>&amp;Exit</source>
        <translation>B&amp;eenden</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="731"/>
        <source>&amp;GUI</source>
        <translation>&amp;GUI</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="743"/>
        <source>Close all content</source>
        <translation>Gesamten Inhalt schließen</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="748"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="749"/>
        <source>P&amp;review</source>
        <translation>Vo&amp;rschau</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="754"/>
        <source>&amp;Log</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="768"/>
        <source>&amp;Settings</source>
        <translation>Ein&amp;stellungen</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="787"/>
        <location filename="../../src/gui/MainWindow.cc" line="788"/>
        <location filename="../../src/gui/MainWindow.cc" line="794"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="790"/>
        <location filename="../../src/gui/MainWindow.cc" line="796"/>
        <source>&amp;About</source>
        <translation>&amp;Über</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="835"/>
        <source>Save INI</source>
        <translation>INI speichern</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="838"/>
        <source>Save INI file as</source>
        <translation>INI speichern als</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="841"/>
        <source>Preview INI</source>
        <translation>Vorschau öffnen</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="845"/>
        <source>Clear INI settings</source>
        <translation>INI-Schlüssel zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../src/gui/MainWindow.cc" line="975"/>
        <source>Open an application or simulation before opening INI files.</source>
        <translation>Eine Anwendung oder Simulation muss erst geöffnet werden.</translation>
    </message>
</context>
<context>
    <name>Number</name>
    <message>
        <location filename="../../src/panels/Number.cc" line="100"/>
        <source>XML error: unknown number format for key &quot;%1::%2&quot;</source>
        <translation>XML-Fehler: Unbekanntes Format für Zahl &quot;%1::%2&quot;</translation>
    </message>
    <message>
        <location filename="../../src/panels/Number.cc" line="134"/>
        <source>Enter an expression such as ${other_ini_key}, ${env:my_env_var} or ${{arithm. expression}}</source>
        <translation>Eingabe eines Ausdrucks wie z. B. ${anderer_ini_schluessel}, ${env:meine_umgebungsvariable} oder ${{arithm. Austruck}}</translation>
    </message>
    <message>
        <location filename="../../src/panels/Number.cc" line="266"/>
        <source>XML error: Could not extract precision for Number key &quot;%1::%2&quot;</source>
        <translation>XML-Fehler: Präzision konnte für den Schlüssel &quot;%1::%2&quot; nicht extrahiert werden</translation>
    </message>
    <message>
        <location filename="../../src/panels/Number.cc" line="279"/>
        <source>XML error: Could not parse minimum double value for key &quot;%1::%2&quot;</source>
        <translation>XML-Fehler: Minimum konnte für den Schlüssel &quot;%1::%2&quot; nicht als Double extrahiert werden</translation>
    </message>
    <message>
        <location filename="../../src/panels/Number.cc" line="283"/>
        <source>XML error: Could not parse maximum double value for key &quot;%1::%2&quot;</source>
        <translation>XML-Fehler: Maximum konnte für den Schlüssel &quot;%1::%2&quot; nicht als Double extrahiert werden</translation>
    </message>
    <message>
        <location filename="../../src/panels/Number.cc" line="306"/>
        <location filename="../../src/panels/Number.cc" line="310"/>
        <source>XML error: Could not parse maximum integer value for key &quot;%1::%2&quot;</source>
        <translation>XML-Fehler: Minimum konnte für den Schlüssel &quot;%1::%2&quot; nicht als Integer extrahiert werden</translation>
    </message>
    <message>
        <location filename="../../src/panels/Number.cc" line="421"/>
        <source>Expression has correct syntax</source>
        <translation>Ausdruck korrekt</translation>
    </message>
    <message>
        <location filename="../../src/panels/Number.cc" line="421"/>
        <source>Expression has wrong syntax</source>
        <translation>Ausdruck hat die falsche Form</translation>
    </message>
    <message>
        <location filename="../../src/panels/Number.cc" line="470"/>
        <source>Could not convert INI value to integer for key &quot;%1::%2&quot;</source>
        <translation>Konnte INI-Wert für &quot;%1::%2&quot; nicht nach Integer konvertieren</translation>
    </message>
    <message>
        <location filename="../../src/panels/Number.cc" line="472"/>
        <location filename="../../src/panels/Number.cc" line="491"/>
        <source>Invalid numeric INI value</source>
        <translation>Invalider numerischer INI-Wert</translation>
    </message>
    <message>
        <location filename="../../src/panels/Number.cc" line="476"/>
        <source>Integer INI value out of range for key &quot;%1::%2&quot; - truncated</source>
        <translation>Integer-Wert außerhalb der Limits für Schlüssel &quot;%1::%2&quot; - gekürzt.</translation>
    </message>
    <message>
        <location filename="../../src/panels/Number.cc" line="478"/>
        <location filename="../../src/panels/Number.cc" line="497"/>
        <source>Truncated numeric INI value</source>
        <translation>Numerischer INI-Wert wurde begrenzt</translation>
    </message>
    <message>
        <location filename="../../src/panels/Number.cc" line="489"/>
        <source>Could not convert INI value to double for key &quot;%1::%2&quot;</source>
        <translation>Konnte INI-Wert für &quot;%1::%2&quot; nicht nach Double konvertieren</translation>
    </message>
    <message>
        <location filename="../../src/panels/Number.cc" line="495"/>
        <source>Double INI value out of range for key &quot;%1::%2&quot; - truncated</source>
        <translation>Double-Wert außerhalb der Limits für Schlüssel &quot;%1::%2&quot; - gekürzt.</translation>
    </message>
</context>
<context>
    <name>PreviewEdit</name>
    <message>
        <location filename="../../src/gui/PreviewEdit.cc" line="45"/>
        <source>Cut (whole line on empty selection)</source>
        <translation>Ausschneiden (ganze Zeile bei leerer Auswahl)</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewEdit.cc" line="46"/>
        <source>Delete to the end of the line</source>
        <translation>Lösche bis zum Ende der Zeile</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewEdit.cc" line="47"/>
        <source>Undo</source>
        <translation>Schritt zurück</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewEdit.cc" line="48"/>
        <source>Redo</source>
        <translation>Schritt vorwärts</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewEdit.cc" line="49"/>
        <source>Add tab</source>
        <translation>Neuen tab hinzufügen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewEdit.cc" line="50"/>
        <source>Move to next tab</source>
        <translation>Zu nächstem Tab springen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewEdit.cc" line="51"/>
        <source>Move to previous tab</source>
        <translation>Zu vorherigem Tab springen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewEdit.cc" line="52"/>
        <source>Find text</source>
        <translation>Text suchen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewEdit.cc" line="53"/>
        <source>Select next word</source>
        <translation>Das nächste Wort selektieren</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewEdit.cc" line="54"/>
        <source>Some supported editor shortcuts:

</source>
        <translation>Ausgewählte verfügbare Tastaturkürzel:

</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewEdit.cc" line="139"/>
        <source>Drop files over the menu or tab titles to open.</source>
        <translation>Ziehe Dateien über das Menü um sie zu öffnen.</translation>
    </message>
</context>
<context>
    <name>PreviewWindow</name>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="96"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="134"/>
        <source>#Empty INI file
</source>
        <translation>#Leere INI-Datei
</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="135"/>
        <source>Open an application and load an INI file to view contents</source>
        <translation>Öffnen Sie eine Anwendung und laden Sie eine INI-Datei um den Inhalt anzuzeigen.</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="269"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="270"/>
        <source>&amp;Open...</source>
        <translation>&amp;Öffnen...</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="275"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="279"/>
        <source>Save &amp;as...</source>
        <translation>Speichern &amp;als...</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="284"/>
        <source>Save and load into GUI</source>
        <translation>Speichern und in Oberfläche laden</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="287"/>
        <source>Load into GUI</source>
        <translation>In Oberfläche laden</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="291"/>
        <source>Quicksave backup</source>
        <translation>Schnelles Backup</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="295"/>
        <source>&amp;Close preview</source>
        <translation>Vorschau s&amp;chließen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="301"/>
        <source>&amp;Edit</source>
        <translation>Änd&amp;ern</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="302"/>
        <source>Undo</source>
        <translation>Schritt zurück</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="306"/>
        <source>Redo</source>
        <translation>Schritt vorwärts</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="311"/>
        <source>Cut</source>
        <translation>Ausschneiden</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="315"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="319"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="323"/>
        <source>Paste to new line</source>
        <translation>In neue Zeile einfügen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="328"/>
        <source>Select all</source>
        <translation>Alles auswählen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="333"/>
        <source>&amp;Find text...</source>
        <translation>Text &amp;suchen...</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="339"/>
        <source>&amp;Insert</source>
        <translation>E&amp;infügen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="340"/>
        <source>Comment header</source>
        <translation>Kommentarblock</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="345"/>
        <source>Missing keys for GUI</source>
        <translation>Fehlende Schlüssel der Oberfläche</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="349"/>
        <source>Mandatory keys for GUI</source>
        <translation>Notwendige Schlüssel</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="356"/>
        <source>&amp;Transform</source>
        <translation>&amp;Transformieren</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="358"/>
        <source>Whitespaces</source>
        <translation>Leerzeichen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="361"/>
        <source>To single spaces</source>
        <translation>Einzelne Leerzeichen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="365"/>
        <source>Adapt to longest keys</source>
        <translation>Den Schlüssellängen anpassen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="371"/>
        <source>Sort</source>
        <translation>Sortieren</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="374"/>
        <source>Alphabetically</source>
        <translation>Alphabetisch</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="378"/>
        <source>In order of INI file</source>
        <translation>Geordnet laut INI-Datei</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="383"/>
        <source>Capitalization</source>
        <translation>Großschreibung</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="386"/>
        <source>Sections to upper case</source>
        <translation>Sektionen groß schreiben</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="391"/>
        <source>Sections to lower case</source>
        <translation>Sektionen klein schreiben</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="396"/>
        <source>Keys to upper case</source>
        <translation>Schlüssel groß schreiben</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="401"/>
        <source>Keys to lower case</source>
        <translation>Schlüssel klein schreiben</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="406"/>
        <source>Values to upper case</source>
        <translation>Werte groß schreiben</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="411"/>
        <source>Values to lower case</source>
        <translation>Werte klein Schreiben</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="417"/>
        <source>All to upper case</source>
        <translation>Alles groß schreiben</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="422"/>
        <source>All to lower case</source>
        <translation>Alles klein schreiben</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="427"/>
        <source>Comments</source>
        <translation>Kommentare</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="430"/>
        <source>Comment selection</source>
        <translation>Ausgewählten Bereich kommentieren</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="436"/>
        <source>Uncomment selection</source>
        <translation>Ausgewählten Bereich ent-kommentieren</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="443"/>
        <source>Comment all content</source>
        <translation>Allen Inhalt kommentieren</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="448"/>
        <source>Duplicate all to comment</source>
        <translation>Inhalt in Kommentar kopieren</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="454"/>
        <source>Move next to values</source>
        <translation>Zu Schlüsseln rücken</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="459"/>
        <source>Collect at bottom</source>
        <translation>Am Dateiende sammeln</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="464"/>
        <source>Trim</source>
        <translation>Leerzeichen am Anfang und Ende entfernen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="468"/>
        <source>Delete all</source>
        <translation>Alle löschen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="473"/>
        <source>Switch to #</source>
        <translation>Zu # wechseln</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="477"/>
        <source>Switch to ;</source>
        <translation>Zu ; wechseln</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="482"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="485"/>
        <source>To original INI on file system</source>
        <translation>Originaldatei am Computer</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="490"/>
        <source>To full INI with GUI keys</source>
        <translation>Mit Werten der Oberfläche</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="496"/>
        <source>&amp;Convert</source>
        <translation>K&amp;onvertieren</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="499"/>
        <source>8 spaces to tabs</source>
        <translation>8 Leerzeichen zu Tabulator</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="503"/>
        <source>4 spaces to tabs</source>
        <translation>4 Leerzeichen zu Tabulator</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="507"/>
        <source>Tabs to 8 spaces</source>
        <translation>Tabulator zu 8 Leerzeichen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="511"/>
        <source>Tabs to 4 spaces</source>
        <translation>Tabulator zu 4 Leerzeichen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="517"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="518"/>
        <source>Show &amp;whitespaces</source>
        <translation>&amp;Leerzeichen anzeigen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="525"/>
        <source>&amp;Syntax highlighting</source>
        <translation>&amp;Texthervorhebung</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="533"/>
        <source>&amp;New tab</source>
        <translation>&amp;Neuer Reiter</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="537"/>
        <source>&amp;Close tab</source>
        <translation>Reiter s&amp;chließen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="545"/>
        <source>&amp;?</source>
        <translation>&amp;?</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="547"/>
        <location filename="../../src/gui/PreviewWindow.cc" line="553"/>
        <source>Preview &amp;help</source>
        <translation>&amp;Hilfe zur Vorschau</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="661"/>
        <source>Preview INI not saved yet</source>
        <translation>Vorschau-INI noch nicht gespeichert</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="662"/>
        <source>Your INI file preview(s) may contain unsaved changes.</source>
        <translation>INI-Preview könnte(n) ungespeicherte Änderungen enthalten</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="622"/>
        <source>Save and load into </source>
        <translation>Speichere und lade in </translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="623"/>
        <source>Load into </source>
        <translation>Lade in </translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="624"/>
        <source>Missing keys for </source>
        <translation>Fehlende Schlüssel für </translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="625"/>
        <source>Mandatory keys for </source>
        <translation>Notwendige Schlüssel für </translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="626"/>
        <source>To full INI with %1 keys</source>
        <translation>Mit Werten der %1-Oberfläche</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="637"/>
        <source>Could not open %1</source>
        <translation>Konnte Datei %1 nicht öffnen</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="649"/>
        <source>Saved to </source>
        <translation>Gespeichert als </translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="759"/>
        <source>Inserted %1 keys</source>
        <translation>%1 Schlüssel hinzugefügt</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="1056"/>
        <source>Open INI file in preview</source>
        <translation>Öffne INI-Datei in Vorschau</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="1084"/>
        <source>Save INI file</source>
        <translation>INI-Datei speichern</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="1121"/>
        <source>Could not open INI file for writing</source>
        <translation>INI-Datei konnte nicht geschrieben werden</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="1127"/>
        <source>Saved to %1</source>
        <translation>Gespeichert in %1</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="1194"/>
        <source>Note: sort first, then start editing.</source>
        <translation>Beachte: Erst sortieren, dann weiter bearbeiten.</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="1266"/>
        <source>Reset to file contents without GUI values.</source>
        <translation>Auf Originaldatei ohne Schlüssel der Oberfläche zurücksetzen.</translation>
    </message>
    <message>
        <location filename="../../src/gui/PreviewWindow.cc" line="1269"/>
        <source>Reset to state of latest preview.</source>
        <translation>Zurücksetzen auf Status der letzten Vorschau</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../../src/main/settings.cc" line="92"/>
        <source>Could not open settings file for writing</source>
        <translation>Konnte Einstellungsdatei nicht schreiben</translation>
    </message>
    <message>
        <location filename="../../src/panels/panels.cc" line="86"/>
        <source>XML error: A parameter in the XML file is missing its type.</source>
        <translation>XML Fehler: Ein XML-Parameter besitzt keinen Typen.</translation>
    </message>
    <message>
        <location filename="../../src/panels/panels.cc" line="89"/>
        <source>XML error: Unknown parameter object in XML file: &quot;%1&quot; for &quot;%2::%3&quot;</source>
        <translation>XML-Fehler: Unbekanntes parameter-Objekt in Datei &quot;%1&quot; für &quot;%2::%3&quot;</translation>
    </message>
    <message>
        <location filename="../../src/panels/panels.cc" line="103"/>
        <source>XML error: Additional default value &quot;%1&quot; ignored because defaults were already set in options for key &quot;%2&quot;.</source>
        <translation>XML-Fehler: Default-Wert &quot;%&quot; ignoriert da schon in Option für Schlüssel &quot;%2&quot; gesetzt.</translation>
    </message>
    <message>
        <location filename="../../src/main/settings.cc" line="72"/>
        <source>Could not read settings file. Unable to load &quot;</source>
        <translation>Konnte Einstellungsdatei nicht öffnen: &quot;</translation>
    </message>
    <message>
        <location filename="../../src/main/settings.cc" line="74"/>
        <source>If possible, the settings file will be recreated for the next program start (check INIshell&apos;s write access to the directory).
If not, INIshell will function normally but will not be able to save any settings.</source>
        <translation>Wenn möglich wird INIshell die Einstellungsdatei beim nächsten Programmstart neu erstellen (bitte prüfen Sie die Schreibrechte).
Andernfalls funktioniert INIshell wie gewohnt, wird aber keine Einstellungen speichern.</translation>
    </message>
    <message>
        <location filename="../../src/main/XMLReader.cc" line="53"/>
        <source>XML error: Could not open file &quot;%1&quot; for reading (%2)</source>
        <translation>XML-Fehler: Konnte Datei &quot;%1&quot; nicht zum Lesen öffnen (%2)</translation>
    </message>
    <message>
        <location filename="../../src/main/main.cc" line="228"/>
        <source>To output a file with &quot;-o&quot; you need to specify the input file with &quot;-i&quot;</source>
        <translation>Um eine Datei mittels &quot;-o&quot; auszugeben muss eine Eingabe-Datei mittels &quot;-i&quot; übergeben werden</translation>
    </message>
    <message>
        <location filename="../../src/main/main.cc" line="233"/>
        <source>To input a file with &quot;-i&quot; you need to specify the output file with &quot;-o&quot;</source>
        <translation>Um eine Eingabe-Datei mittels &quot;-i&quot; zu verarbeiten muss eine Ausgabe-Datei mittels &quot;-o&quot; festgelegt werden</translation>
    </message>
    <message>
        <location filename="../../src/main/main.cc" line="258"/>
        <source>Unable to open output INI file &quot;%1&quot;: %2</source>
        <translation>Konnte INI-Datei &quot;%1&quot; nicht für Ausgabe öffnen: %2</translation>
    </message>
    <message>
        <location filename="../../src/main/inishell.cc" line="306"/>
        <source>XML error: referenced panel &quot;%1&quot; not found (requested via &quot;%2&quot;).</source>
        <translation>XML Fehler: Panel &quot;%1&quot; wurde nicht gefunden (benötigt von &quot;%2&quot;).</translation>
    </message>
    <message>
        <location filename="../../src/main/inishell.cc" line="311"/>
        <source>XML error: referenced panel &quot;%1&quot; found %2 times - picking any (requested via &quot;%3&quot;).</source>
        <translation>XML Fehler: Referenziertes panel &quot;%1&quot; %2&quot; mal gefunden - verwende ein beliebiges (benötigt von &quot;%3&quot;).</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <location filename="../../src/main/XMLReader.cc" line="75"/>
        <source>XML error: %1 (line %2, column %3)</source>
        <translation>XML Fehler: %1 (Zeile %2, Spalte %3)</translation>
    </message>
    <message>
        <location filename="../../src/main/XMLReader.cc" line="117"/>
        <source>XML error: Replacing a node failed for parametergroup &quot;%1&quot;.</source>
        <translation>XML-Fehler: Knoten-Reproduktion für parametergroup &quot;%1&quot; fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../../src/main/XMLReader.cc" line="127"/>
        <source>XML error: Replacement parametergroup &quot;%1&quot; not found.</source>
        <translation>XML-Fehler: parametergroup &quot;%1&quot; wurde nicht zum Ersetzen gefunden.</translation>
    </message>
    <message>
        <location filename="../../src/main/XMLReader.cc" line="173"/>
        <source>XML error: [Include file &quot;%1&quot;] %2 (line %3, column %4)</source>
        <translation>XML-Fehler: [Include-Datei &quot;%1&quot;] %2 (Zeile %3, Spalte %4)</translation>
    </message>
    <message>
        <location filename="../../src/main/XMLReader.cc" line="187"/>
        <source>XML error: Replacing a node failed for inclusion system in master file &quot;%1&quot;</source>
        <translation>XML-Fehler: Ersetzung eines Knotens fehlgeschlagen in Master-XML &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../src/main/XMLReader.cc" line="192"/>
        <source>XML error: Unable to open XML include file &quot;%1&quot; for reading (%2)
</source>
        <translation>XML Fehler: Die zu inkludierende Datei &quot;%1&quot; konnte nicht gelesen werden (%2)
</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../../src/main/Error.cc" line="132"/>
        <source>Info</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../../src/main/Error.cc" line="137"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="../../src/main/Error.cc" line="142"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../../src/main/Error.cc" line="147"/>
        <source>Critical Error</source>
        <translation>Kritischer Fehler</translation>
    </message>
    <message>
        <location filename="../../src/main/Error.cc" line="152"/>
        <source>Fatal Error</source>
        <translation>Fataler Fehler</translation>
    </message>
    <message>
        <location filename="../../src/main/Error.cc" line="87"/>
        <source>Aborted after fatal error:</source>
        <translation>Absturtz nach fatalem Fehler:</translation>
    </message>
</context>
<context>
    <name>RememberDialog</name>
    <message>
        <location filename="../../src/gui/RememberDialog.cc" line="48"/>
        <source>Don&apos;t show this warning again</source>
        <translation>Diese Warnung nicht mehr anzeigen</translation>
    </message>
    <message>
        <location filename="../../src/gui/RememberDialog.cc" line="49"/>
        <source>The warning can be re-enabled in the settings</source>
        <translation>Die Warnung kann in den Einstellungen wieder aktiviert werden</translation>
    </message>
</context>
<context>
    <name>SectionButton</name>
    <message>
        <location filename="../../src/gui/SectionButton.cc" line="51"/>
        <source>Click to add another instance of this section to the INI file</source>
        <translation>Erzeuge Kopie dieser INI-Sektion</translation>
    </message>
    <message>
        <location filename="../../src/gui/SectionButton.cc" line="55"/>
        <source>Click to remove this section instance from the INI file</source>
        <translation>Entferne diese Kopie der INI-Sektion</translation>
    </message>
</context>
<context>
    <name>SectionTab</name>
    <message>
        <location filename="../../src/gui/SectionTab.cc" line="351"/>
        <source>INI settings will be lost</source>
        <translation>INI-Einstellungen gehen verloren</translation>
    </message>
    <message>
        <location filename="../../src/gui/SectionTab.cc" line="352"/>
        <source>If you close this tab, all of its settings will be lost.</source>
        <translation>Wenn dieser Tab geschlossen wird, gehen dessen Einstellungen verloren.</translation>
    </message>
    <message>
        <location filename="../../src/gui/SectionTab.cc" line="353"/>
        <source>If you did not make any changes to the default values of this tab/section, you can safely ignore this message.</source>
        <translation>Wenn Sie keine Änderungen zu den Default-Werten vorgenommen haben können Sie diese Warnung ignorieren.</translation>
    </message>
</context>
<context>
    <name>Selector</name>
    <message>
        <location filename="../../src/panels/Selector.cc" line="70"/>
        <source>XML error: Selector panel &quot;%1&quot; must not have a key.</source>
        <translation>XML-Fehler: Selector &quot;%1&quot; sollte keinen Schlüssel besitzen.</translation>
    </message>
    <message>
        <location filename="../../src/panels/Selector.cc" line="161"/>
        <source>XML error: No template panel given for key &quot;%1::%2&quot;</source>
        <translation>XML-Fehler: Kein template-Element vorhanden für Schlüssel &quot;%1::%2&quot;</translation>
    </message>
    <message>
        <location filename="../../src/panels/Selector.cc" line="192"/>
        <source>Empty text field</source>
        <translation>Leeres Textfeld</translation>
    </message>
    <message>
        <location filename="../../src/panels/Selector.cc" line="196"/>
        <source>Item already exists</source>
        <translation>Wert existiert bereits</translation>
    </message>
    <message>
        <location filename="../../src/panels/Selector.cc" line="256"/>
        <source>XML error: Replacing a node failed for Selector &quot;%1&quot;.</source>
        <translation>XML-Fehler: Ein XML-Knoten konnte im Selector &quot;%1&quot; nicht ersetzt werden.</translation>
    </message>
    <message>
        <location filename="../../src/panels/Selector.cc" line="281"/>
        <source>Item &quot;%1&quot; does not exist</source>
        <translation>Eintrag &quot;%1&quot; existiert nicht</translation>
    </message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <location filename="../../src/gui/SettingsWindow.cc" line="53"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../src/gui/SettingsWindow.cc" line="54"/>
        <source>Reset all INIshell settings to default values</source>
        <translation>Alle INI-Einstellungen auf Standardwerte zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../src/gui/SettingsWindow.cc" line="58"/>
        <source>&amp;Cancel</source>
        <translation>Abbre&amp;chen</translation>
    </message>
    <message>
        <location filename="../../src/gui/SettingsWindow.cc" line="61"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="../../src/gui/SettingsWindow.cc" line="83"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../../src/gui/SettingsWindow.cc" line="246"/>
        <source>&lt;b&gt;Appearance settings changed&lt;/b&gt;</source>
        <translation>&lt;b&gt;Aussehen wurde geändert&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../src/gui/SettingsWindow.cc" line="248"/>
        <source>A restart of INIshell is required for appearance settings to take effect. Restart now?

Make sure you have all your work saved!</source>
        <translation>INIshell muss neu gestarted werden um das Aussehen zu aktualisieren. Jetzt neu starten?

Speichern Sie zuerst Ihre Arbeit!</translation>
    </message>
</context>
<context>
    <name>Spacer</name>
    <message>
        <location filename="../../src/panels/Spacer.cc" line="48"/>
        <source>XML error: Could not parse height for spacer element</source>
        <translation>XML-Fehler: Konnte die Höhe für spacer-Element nicht extrahieren</translation>
    </message>
    <message>
        <location filename="../../src/panels/Spacer.cc" line="56"/>
        <source>XML error: Could not parse width for spacer element</source>
        <translation>XML-Fehler: Konnte die Breite für spacer-Element nicht extrahieren</translation>
    </message>
</context>
<context>
    <name>SyntaxHighlighter</name>
    <message>
        <location filename="../../src/main/SyntaxHighlighter.cc" line="272"/>
        <source>(truncated - right-click to show more)</source>
        <translation>(gekürzt - rechts-klicken um mehr der Datei zu zeigen)</translation>
    </message>
</context>
<context>
    <name>TerminalView</name>
    <message>
        <location filename="../../src/gui/TerminalView.cc" line="46"/>
        <source>Workflow console output</source>
        <translation>Workflow-Ausgabe</translation>
    </message>
    <message>
        <location filename="../../src/gui/TerminalView.cc" line="93"/>
        <location filename="../../src/gui/TerminalView.cc" line="96"/>
        <source>Clear</source>
        <translation>Alles löschen</translation>
    </message>
</context>
<context>
    <name>Textfield</name>
    <message>
        <location filename="../../src/panels/Textfield.cc" line="155"/>
        <source>Wrong format for the expression</source>
        <translation>Ausdruck hat falsches Format</translation>
    </message>
    <message>
        <location filename="../../src/panels/Textfield.cc" line="155"/>
        <location filename="../../src/panels/Textfield.cc" line="166"/>
        <source>Valid expression</source>
        <translation>Ausdruck korrekt</translation>
    </message>
    <message>
        <location filename="../../src/panels/Textfield.cc" line="166"/>
        <source>Show online map</source>
        <translation>Zeige Onlinekarte</translation>
    </message>
    <message>
        <location filename="../../src/panels/Textfield.cc" line="167"/>
        <source>Wrong format for the provided coordinates</source>
        <translation>Falsches Format für Koordinaten</translation>
    </message>
    <message>
        <location filename="../../src/panels/Textfield.cc" line="167"/>
        <source>Wrong format for the provided value</source>
        <translation>Wert hat falsches Format</translation>
    </message>
</context>
<context>
    <name>WorkflowPanel</name>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="65"/>
        <location filename="../../src/gui/WorkflowPanel.cc" line="73"/>
        <source>Applications</source>
        <translation>Anwendungen</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="66"/>
        <location filename="../../src/gui/WorkflowPanel.cc" line="75"/>
        <source>Simulations</source>
        <extracomment>Translation hint: This is for the workflow panel on the left side.</extracomment>
        <translation>Simulationen</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="69"/>
        <source>Open an application or simulation before opening INI files.</source>
        <translation>Eine Anwendung oder Simulation muss erst geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="76"/>
        <source>INI files</source>
        <translation>INI-Dateien</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="118"/>
        <source>Scanning for applications and simulations...</source>
        <translation>Suche Anwendungen und Simulationen...</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="121"/>
        <source>Done scanning, </source>
        <translation>Suche beendet, </translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="122"/>
        <source>items</source>
        <translation>Einträge</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="122"/>
        <source>nothing</source>
        <translation>keine</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="122"/>
        <source> found.</source>
        <translation> gefunden.</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="124"/>
        <source>No applications found. Please check the help section &quot;Applicatons&quot; to obtain the necessary files.</source>
        <translation>Keine Anwendungen gefunden: Siehe die Hilfeseite &quot;Applications&quot; um die nötigen Dateien zu erhalten.</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="127"/>
        <source>No simulations found. Please check the help section &quot;Simulations&quot; to set up your simulations.</source>
        <translation>Keine Simulationen gefunden: Siehe die Hilfeseite &quot;Simulations&quot; um die nötigen Dateien zu erhalten.</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="169"/>
        <source>Workflow element &quot;%1&quot; unknown</source>
        <translation>Workflow-Element &quot;%1&quot; ist nicht bekannt</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="182"/>
        <source>Set working directory from:</source>
        <translation>Arbeitsverzeichnis:</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="256"/>
        <source>Pick a date/time</source>
        <translation>Datum/Zeit wählen</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="292"/>
        <source>No command given for button &quot;%1&quot; (ID: &quot;%2&quot;)</source>
        <translation>Der Knopf &quot;%1&quot; mit der ID &quot;%2&quot; hat kein Kommando assoziiert</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="293"/>
        <source>No command</source>
        <translation>Kein Kommando</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="350"/>
        <source>Could not check application file: unable to read &quot;%1&quot; (%2)</source>
        <translation>Konnte Applikations-Datei &quot;%1&quot; nicht lesen (%2)</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="425"/>
        <source>Multiple elements found for ID &quot;%1&quot;</source>
        <translation>Mehrere Elemente für ID &quot;%1&quot; gefunden</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="427"/>
        <source>Element ID &quot;%1&quot; not found</source>
        <translation>Element mit der ID &quot;%1&quot; nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="458"/>
        <source>Empty INI file - you need to save first</source>
        <translation>Leere INI-Datei - es muss erst gepseichert werden</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="471"/>
        <source>INI key must be SECTION</source>
        <translation>INI-Schlüssel muss vom folgenden Format sein: SECTION</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="477"/>
        <source>INI key &quot;%1&quot; not found</source>
        <translation>INI-Schlüssel &quot;%1&quot; nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="530"/>
        <source>Path element ID &quot;%1&quot; not found</source>
        <translation>Pfad-Element mit der ID &quot;%1&quot; nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="563"/>
        <source>Button with ID &quot;%1&quot; not found</source>
        <translation>Knopf mit der ID &quot;%1&quot; nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="608"/>
        <source>A process is running...</source>
        <translation>Ein Prozess läuft...</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="667"/>
        <source>The process was terminated unexpectedly (exit code: %1, exit status: %2).</source>
        <translation>Der Prozess wurde unerwartet beendet (exit code: %1, exit status: %2).</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="671"/>
        <location filename="../../src/gui/WorkflowPanel.cc" line="737"/>
        <source>Process was terminated</source>
        <translation>Prozess wurde beendet</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="673"/>
        <source>Process has finished</source>
        <translation>Ein Prozess hat fertig gearbeitet</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="720"/>
        <source>Can not start process. Please make sure that the executable is in the PATH environment variable or in any of the following paths </source>
        <translation>Konnte den Prozess nicht starten. Bitte stellen Sie sicher, dass sich das ausgeführte Programm entweder in der PATH-Systemvariable oder einem der folgenden Pfade befindet: </translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="727"/>
        <source>Timeout when running process...</source>
        <translation>Zeitüberschreitung beim Ausführen eines Prozesses...</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="731"/>
        <source>Can not read or write to process.</source>
        <translation>Ein Prozess konnte nicht gelesen oder beschrieben werden.</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="734"/>
        <source>Unknown error when running process.</source>
        <translation>Unbekannter Fehler bei der Ausführung eines Prozesses.</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="554"/>
        <source>A button can not click itself</source>
        <translation>Ein Knopf darf sich nicht selbst drücken</translation>
    </message>
    <message>
        <location filename="../../src/gui/WorkflowPanel.cc" line="582"/>
        <source>Stop process</source>
        <translation>Prozess anhalten</translation>
    </message>
</context>
</TS>
